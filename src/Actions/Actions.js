/**
 * Acciones del inicio y cierre de sesión.
 */
export const LOGIN_SISTEMA = "LOGIN_SISTEMA";
export const LOGOUT_SISTEMA = "LOGOUT_SISTEMA";
export const LOGOUT_SISTEMA_ERROR = "LOGOUT_SISTEMA_ERROR";

export const LOADING = "LOADING";
export const LOADED = "LOADED";

export const MENU_LOADING = "MENU_LOADING";
export const MENU_LOADED = "MENU_LOADED";

export const DOMICILIO_LOADING = "DOMICILIO_LOADING";
export const DOMICILIO_LOADED = "DOMICILIO_LOADED";