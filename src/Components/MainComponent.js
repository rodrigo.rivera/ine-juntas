import React, { useState, useEffect, useReducer } from "react";
import thunk from "redux-thunk";
import "moment/locale/es-us";
import Home from "../PaginasContenedores/Home/Home";
//import RepreConsulta from '../PaginasContenedores/Representantes/Consulta/RepreConsulta';
import axios from "axios";
import { Layout, Icon, Spin } from "antd";
import Cookies from "js-cookie";
import "./MainComponent.scss";
import alertINE from "../Components/Alert/AlertsINE";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { storeMenu, storeLoading, storeDomicilios } from "../Reducers/HomeRed";
import { createStore, applyMiddleware, compose } from "redux";
import HomeFeka from "../PaginasContenedores/Home/homefeka";
import { ReactComponent as Heder_Logo } from "../Assets/Imagenes/Header/Logo.svg";
import { ReactComponent as Heder_Logo_User } from "../Assets/Imagenes/Header/user.svg";
import Footer from "../PaginasContenedores/ContenedorGeneral/Footer/Footer";
import Header from "../PaginasContenedores/ContenedorGeneral/Header/Header";
import jwtDecode from "jwt-decode";
import Menu from "../Components/Sider/Sider";
import ProcessMenu from "../Components/ProcessMenu/ProcessMenu";
import Vocales from "../PaginasContenedores/Vocales/VocalesInit";
import VocalesBaja from "../PaginasContenedores/Vocales/VocalesBaja";
import VocalesMostrar from "../PaginasContenedores/Vocales/VocalesMostrar";
import Crear from "../PaginasContenedores/Vocales/VocalesFormulario";
import DomiciliosHome from "../PaginasContenedores/Domicilios/DomiciliosHome";
// import Formulario from "../PaginasContenedores/Vocales/VocalesFormulario";
import CrearDomicilio from "../PaginasContenedores/Domicilios/CrearDomicilio";
import "../PaginasContenedores/Domicilios/style.scss";

const { Content } = Layout;

const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;

const store = createStore(storeMenu, compose(applyMiddleware(thunk)));
const storeLoad = createStore(storeLoading, compose(applyMiddleware(thunk)))

export default function Main(props) {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [accessToken, setAccesssToken] = useState([]);
  const [dataMenu, setDataMenu] = useState(null);
  const [selectMenuOC, setSelectMenuOC] = useState(false);
  const [urls, setUrls] = useState({});
  //const inicialState = { complete: false, data: null }
  //const [datosMenu, dispatch] = useReducer(reducer, inicialState)

  // useEffect(() => {
  //   axios
  //     .get(window.location.origin + "/JsonHelpers/jsonUrl.json")
  //     .then((response) => {

  //       if (response.status === 200) {
  //         setUrls(response.data);
  //         console.log(response);
  //         localStorage.setItem('aplicaciones', response.data.urlLogin);
  //         localStorage.setItem('junta-WSbackend', response.data.serverConsejo);
  //         localStorage.setItem('junta-wsUrlPath', response.data.wsUrlPath);
  //         localStorage.setItem('junta', response.data.urlJuntas);

  //         // setUrls(response.data);
  //         console.log("urls::::::::::");
  //         console.log(response.data);
  //         if (
  //           Cookies.get("us-ses") === undefined ||
  //           Cookies.get("cn-ses") === undefined ||
  //           Cookies.get("vr-ses") === undefined ||
  //           Cookies.get("sy-ses") === undefined
  //         ) {
  //           console.log("no fue posible encontrar las cookies");
  //           window.location.href =
  //             response.data.urlLogin + "/sesiones/aplicaciones";
  //         } else {
  //           if (Cookies.get("sy-ses") !== response.data.idSistema) {
  //             console.log(
  //               "error en cookies sy-ses " +
  //               Cookies.get("sy-ses") +
  //               "________" +
  //               response.data.idSistema
  //             );
  //             window.location.href =
  //               response.data.urlLogin + "/sesiones/aplicaciones";
  //           } else {
  //             // const dataUrl = {
  //             //   usuario: Cookies.get("us-ses"),
  //             //   password: Cookies.get("cn-ses"),
  //             //   versionAplicacion: Cookies.get("vr-ses"),
  //             //   idSistema: Cookies.get("sy-ses"),
  //             // };

  //             const dataUrl = {
  //               usuario: "eduardo.salmeron",
  //               // usuario: "desa05-01",
  //               password: "luapfeVxf+UvOjMJ9xRZjA==",
  //               versionAplicacion: "1",
  //               idSistema: "227",
  //             };
  //             // console.log("la url  es ");
  //             // console.log(response.data.wsUrlPath);
  //             // console.log(
  //             //   "call :::" + response.data.wsUrlPath + "/ingresaSistema"
  //             // );
  //             // console.log("las cookies::::::::::");
  //             // console.log(dataUrl);
  //             axios
  //               .post(response.data.wsUrlPath + "/ingresaSistema", dataUrl)
  //               .then((response) => {
  //                 console.log(response);
  //                 if (response.status === 200) {
  //                   if (response.data.entity.code === 200) {
  //                     setData(response.data.entity.datosUsuario);
  //                     localStorage.setItem(
  //                       "accessToken",
  //                       response.data.entity.datosUsuario.tknA
  //                     );
  //                     setAccesssToken(response.data.entity.datosUsuario.tknA);
  //                     setDataMenu(response.data.entity.datosUsuario);
  //                     setLoading(false);

  //                     //console.log("respuesta inciioo", response.data.entity.datosUsuario)
  //                   } else {
  //                     console.log("error mainComponente ", response);
  //                     var message =
  //                       response.data.entity.message != undefined
  //                         ? response.data.entity.message
  //                         : "Ha ocurrido un error. Revisa tu conexión a internet.";
  //                     alertINE(
  //                       "error",
  //                       JSON.stringify(response.data.entity.message)
  //                     );
  //                     // alert("serror servidor ", JSON.stringify(response));
  //                     // Cookies.set("mg-ses", response.data.entity.message, {
  //                     //   path: "/",
  //                     //   domain: response.data.cookieDomain,
  //                     // });
  //                     // Cookies.remove("us-ses", {
  //                     //   path: "/",
  //                     //   domain: response.data.cookieDomain,
  //                     // });
  //                     // Cookies.remove("cn-ses", {
  //                     //   path: "/",
  //                     //   domain: response.data.cookieDomain,
  //                     // });
  //                     // Cookies.remove("vr-ses", {
  //                     //   path: "/",
  //                     //   domain: response.data.cookieDomain,
  //                     // });
  //                     // Cookies.remove("sy-ses", {
  //                     //   path: "/",
  //                     //   domain: response.data.cookieDomain,
  //                     // });
  //                     // console.log("response.data.entity.code ");

  //                     // window.location.href =
  //                     //   urls.urlLogin + "/sesiones/aplicaciones";
  //                   }
  //                 }
  //               })
  //               .catch((err) => {
  //                 console.log("erorr inciio session ", err);
  //                 alert("error " + err);
  //                 Cookies.set("mg-ses", err, {
  //                   path: "/",
  //                   domain: response.data.cookieDomain,
  //                 });
  //                 Cookies.remove("us-ses", { path: "/", domain: response.data.cookieDomain });
  //                 Cookies.remove("cn-ses", { path: "/", domain: response.data.cookieDomain });
  //                 Cookies.remove("vr-ses", { path: "/", domain: response.data.cookieDomain });
  //                 Cookies.remove("sy-ses", { path: "/", domain: response.data.cookieDomain });
  //                 console.log("error fatal ::::");
  //                 console.log(err);
  //                 window.location.href =
  //                   urls.urlLogin + "/sesiones/aplicaciones";
  //               });
  //           }
  //         }
  //       }
  //     });
  // }, []);

  
  useEffect(() => {
    axios
      .get(window.location.origin + "/JsonHelpers/jsonUrl.json")
      .then((response) => {
        // console.log("urls::::::::::");
        // console.log(response);
        localStorage.setItem('aplicaciones', response.data.urlLogin);
        if (response.status === 200) {
          setUrls(response.data);
          //console.log(response.data);
          // if (
          //   Cookies.get("us-ses") === undefined ||
          //   Cookies.get("cn-ses") === undefined ||
          //   Cookies.get("vr-ses") === undefined ||
          //   Cookies.get("sy-ses") === undefined
          // ) {
          //   console.log("no fue posible encontrar las cookies");
          //   // window.location.href =
          //   //   response.data.urlLogin + "/sesiones/aplicaciones";
          // } else {
          //   if (Cookies.get("sy-ses") !== response.data.idSistema) {
          //     console.log(
          //       "error en cookies sy-ses " +
          //       Cookies.get("sy-ses") +
          //       "________" +
          //       response.data.idSistema
          //     );
          //     // window.location.href =
          //     //   response.data.urlLogin + "/sesiones/aplicaciones";
          //   } else {
          //     const dataUrl = {
          //       usuario: Cookies.get("us-ses"),
          //       password: Cookies.get("cn-ses"),
          //       versionAplicacion: Cookies.get("vr-ses"),
          //       idSistema: Cookies.get("sy-ses"),
          //     };

          const dataUrl = {
            usuario: "alfonso.rios",
            password: "luapfeVxf+UvOjMJ9xRZjA==",
            versionAplicacion: "1",
            idSistema: "227",
          };
          // console.log("la url  es ");
          // console.log(response.data._wsUrlPath);
          // console.log(
          //   "call :::" + response.data._wsUrlPath + "/ingresaSistema"
          // );
          //console.log("las cookies::::::::::");
          //console.log(dataUrl);
          axios
            .post(response.data.wsUrlPath + "/ingresaSistema", dataUrl)
            .then((response) => {
              //console.log(response);
              if (response.status === 200) {
                if (response.data.entity.code === 200) {
                  setData(response.data.entity.datosUsuario);
                  localStorage.setItem(
                    "accessToken",
                    response.data.entity.datosUsuario.tknA
                  );
                  setAccesssToken(response.data.entity.datosUsuario.tknA);
                  setDataMenu(response.data.entity.datosUsuario);
                  setLoading(false);

                  //console.log("respuesta inciioo", response.data.entity.datosUsuario)
                } else {
                  //console.log("error mainComponente ", response);
                  var message =
                    response.data.entity.message != undefined
                      ? response.data.entity.message
                      : "Ha ocurrido un error. Revisa tu conexión a internet.";
                  alertINE(
                    "error",
                    JSON.stringify(response.data.entity.message)
                  );
                  //alert("serror servidor ", JSON.stringify(response));
                  // Cookies.set("mg-ses", response.data.entity.message, {
                  //   path: "/",
                  //   domain: response.data.cookieDomain,
                  // });
                  // Cookies.remove("us-ses", {
                  //   path: "/",
                  //   domain: response.data.cookieDomain,
                  // });
                  // Cookies.remove("cn-ses", {
                  //   path: "/",
                  //   domain: response.data.cookieDomain,
                  // });
                  // Cookies.remove("vr-ses", {
                  //   path: "/",
                  //   domain: response.data.cookieDomain,
                  // });
                  // Cookies.remove("sy-ses", {
                  //   path: "/",
                  //   domain: response.data.cookieDomain,
                  // });
                  //console.log("response.data.entity.code ");

                  // window.location.href =
                  //   urls.urlLogin + "/sesiones/aplicaciones";
                }
              }
            })
            .catch((err) => {
              console.log("erorr inciio session ", err);
              //alert("error " + err);
              Cookies.set("mg-ses", err, {
                path: "/",
                domain: response.data.cookieDomain,
              });
              Cookies.remove("us-ses", { path: "/", domain: response.data.cookieDomain });
              Cookies.remove("cn-ses", { path: "/", domain: response.data.cookieDomain });
              Cookies.remove("vr-ses", { path: "/", domain: response.data.cookieDomain });
              Cookies.remove("sy-ses", { path: "/", domain: response.data.cookieDomain });
              // console.log("error fatal ::::");
              // console.log(err);
              // window.location.href =
              //   urls.urlLogin + "/sesiones/aplicaciones";
            });
        }
        //    }
        //   }
      });
  }, []);



  /**
   * estado Redux
   */
  store.subscribe(() => {
    let respuestaStore = store.getState();
    //console.log("redux home ", respuestaStore);

    if (!respuestaStore.isLoading) {
      //console.log("Home: ", respuestaStore.menu);
      if (respuestaStore.menu.infoMenu.estadoSelec != null) {
        if (respuestaStore.menu.infoMenu.estadoSelec.idEstado === 0) {
          setDataMenu(respuestaStore.menu);
          setSelectMenuOC(true);
        } else if (axuliarDistrito(respuestaStore.menu) != null)
          setDataMenu(respuestaStore.menu);
      }
    }
  });

  /**
   * verifica que aya distrito
   * @param {*} menu
   */
  const axuliarDistrito = (menu) => {
    setSelectMenuOC(false);
    let tempDistrito = null;
    //console.log("auxiliar ", menu.infoMenu)
    if (menu.infoMenu.distritoFedSelec != null) {
      //console.log("entro a el axuli ",)
      tempDistrito = menu.infoMenu.distritoFedSelec.idDistrito;
    }
    if (menu.infoMenu.distritoLocSelec != null) {
      tempDistrito = menu.infoMenu.distritoLocSelec;
    }
    if (menu.infoMenu.municipioSelec != null) {
      tempDistrito = menu.infoMenu.municipioSelec;
    }
    return tempDistrito;
  };
  return (
    <div style={{paddingLeft:"50px", paddingRight:"30px"}}>
      {loading ? (
        <div className="loading-style">
          <Spin indicator={antIcon} tip="Cargando Sistema de Junta..."></Spin>
        </div>
        //<Formulario />
        
      ) : (
          <Layout style={{ minHeight: "100vh" }}>
            <Header
              //Logo={Heder_Logo}
              //LogoUser={Heder_Logo_User}
              User={jwtDecode(accessToken).sub}
              inicio=""
              //data={data}
              accessToken={accessToken}
            />
            <Layout>
              <Router>
                {
                  (dataMenu.infoMenu.listMenu.length > 0 || selectMenuOC)
                    ?
                    (
                      <Menu menu={dataMenu.infoMenu.listMenu} distritoFedSelec={dataMenu.infoMenu.distritoFedSelec} store={store} />
                    )
                    :
                    null
                }

                <Content style={{ background: "#fff", paddingLeft:"30px", paddingBottom: "2.5rem" }}>
                  {dataMenu != null ? (
                    <ProcessMenu store={store} storeLoad={storeLoad} isLoadingMainMenu={loading} dataMenu={dataMenu} />
                  ) : null}



                  {/* 
                se inserta el  la direcciones dependiendo el componente 
                 */}
                 
                
                  <Route
                    path="/vocales"
                    component={({ match }) => (
                      <Vocales match={match} menu={dataMenu} store={storeLoad} />
                    )}
                  />
                  <Route
                    path="/DomicilioJunta"
                    component={({ match }) => (
                      <DomiciliosHome match={match} dataMenu={dataMenu} store={storeLoad} />
                    )}
                  />
                </Content>
              </Router>
            </Layout>

            <Footer className="footer" />
          </Layout>
        )}
    </div>
  );
}
