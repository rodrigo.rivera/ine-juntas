import React, { useContext, useState, useEffect, useRef } from "react";
import { DownloadOutlined, FormOutlined } from '@ant-design/icons';
import Estados from "../../Assets/files/Estados.json";
import alertINE from "../Alert/AlertsINE";
import "antd/dist/antd.css";

import {
    Table,
    Input,
    Button,
    Popconfirm,
    Form,
    Select,
    DatePicker,
    Radio
} from "antd";
import moment from "moment";
const { Option } = Select;

export default function Experiencia(props) {
    

    const { getData, getDataSend, listExperiencia,listExperienciaSend ,modifcar,mostrar} = props;
    console.log("experiencia",mostrar.enviar)
    

    const [llenarDistrito, setLlenarDistrito] = useState(null);
    const [dataSourceSend, setDataSourceSend] = useState(listExperienciaSend != null ? listExperienciaSend : (!modifcar? mostrar.enviar:[]));
    const [dataSource, setDataSource] = useState(listExperiencia != null ? listExperiencia : (!modifcar? mostrar.mostrar:[]));
    const [editar, setEditar] = useState({ editar: false, id: null });
    const [count, setCount] = useState(dataSource != null ? dataSource.length + 1 : 0);
    const [fechaI, setFechaI] = useState({ estatus: false, value: null, name: "" });
    const [fechaT, setFechaT] = useState({ estatus: false, value: null, name: "" });
    const [rama, setRama] = useState({ estatus: false, value: null, name: "" });
    const [puesto, setPuesto] = useState({ estatus: false, value:null, name: "" });
    const [instituto, setInstituto] = useState({
        estatus: false,
        value:null,
        name: ""
    });
    const [puestos, setPuestos] = useState(props.puestos);
    const [entidad, setEntidad] = useState({
        estatus: false,
        value:null,
        name: ""
    });
    const [distrito, setDistrito] = useState({
        estatus: false,
        value:null,
        name: ""
    });

    useEffect(() => {
        getDataSend(dataSourceSend)
        getData(dataSource)
    }, [dataSource])

    const columns = [
        {
            title: "Fecha inicio",
            dataIndex: "fechaIncio",
            width: "12%"
        },
        {
            title: "Fecha término",
            dataIndex: "fechaTermino",
            width: "12%"
        },
        {
            title: "Rama",
            dataIndex: "rama",
            width: "12%"

        },
        {
            title: "Puesto",
            dataIndex: "puesto",
            width: "12%"
        },
        {
            title: "Instituto",
            dataIndex: "instituto",
            width: "12%"
        },
        {
            title: "Entidad",
            dataIndex: "entidad",
            width: "12%"
        },

        {
            title: "Distrito",
            dataIndex: "distrito",
            width: "12%"
        },
        {
            title: "Acciones",
            dataIndex: "operation",
            width: "12%",
            render: (text, record) =>
                dataSource.length > 0 ? (
                    <>
                        <Popconfirm
                            title="Deseas eliminar el elemento"
                            onConfirm={() => handleDelete(record.key)}
                        >
                            <Button
                            key={JSON.stringify(record.key)}
                            shape="circle"
                            className=".ant-btn ant-btn-modifica"
                            style={{ marginLeft: '15px' }}
                            >

                            <FormOutlined className="iconDelete" size={20} />
                             </Button>
                        </Popconfirm>
                        <Popconfirm
                            title="Deseas Editar el elemento"
                            onConfirm={() => handlEdit(record.key)}
                        >
                            <Button
                            key={JSON.stringify(record.key)}
                            shape="circle"
                            className=".ant-btn ant-btn-modifica"
                            style={{ marginLeft: '15px' }}
                            >
                            <FormOutlined className="iconEdit" size={20} />

                            </Button>
                        </Popconfirm>
                    </>
                ) : null
        }
    ];

    const buscar=(id)=>{
        let index=null;
        for (let i = 0; i < dataSource.length; i++) {
            if(dataSourceSend[i].key==id){
                index=i;
            }
            
        }
        return index;
    }
    const handlEdit = key => {
        
        let pos  =buscar(key)
        console.log("la llave ", key,"== posisicon",pos );
        let temp = dataSource[buscar(key)];
        let tempIndex = dataSourceSend[buscar(key)];
        console.log("valor ", temp)
        if (temp.entidad != undefined) {
            setEntidad({ estatus: true, value: tempIndex.entidad, name: temp.entidad });
        }
        if (temp.rama != undefined) {

            const rama = temp.rama === "Administrativo" ? 1 : 2;

            setRama({ estatus: true, value: tempIndex.rama, name: temp.rama });
        }

        if (temp.instituto != undefined) {
            const instituto = temp.instituto === "INE" ? 1 : 2;
            setInstituto({ estatus: true, value: tempIndex.instituto, name: temp.instituto });
        }
        //const puesto = array1.findIndex((element) => element== 444) //para buscar
        setFechaI({
            estatus: true,
            value: temp.fechaIncio,
            name: temp.fechaIncio
        });
        setFechaT({
            estatus: true,
            value: temp.fechaTermino,
            name: temp.fechaTermino
        });
        if (temp.distrito != undefined) {

            setDistrito({ estatus: true, value: tempIndex.distrito, name: temp.distrito });
        }
        setPuesto({ estatus: true, value: tempIndex.puesto, name: temp.puesto });

        setEditar({ editar: true, id: pos });
    };

    const handleDelete = key => {
        console.log(key);
        setFechaI({ estatus: false, value: null, name: "" });
        setFechaT({ estatus: false, value: null, name: "" });
        setRama({ estatus: false, value: null, name: "" });
        setPuesto({ estatus: false, value: null, name: "" });
        setInstituto({ estatus: false, value: null, name: "" });
        setEntidad({ estatus: false, value: null, name: "" });
        setDistrito({ estatus: false, value: null, name: "" });
        setEditar({ editar: false, id: null });
        setDataSource(dataSource.filter(item => item.key !== key));
        setDataSourceSend(dataSourceSend.filter(item => item.key !== key));

    };

    const handleAdd = () => {
        //console.log("pulse");
        //const { count, dataSource } = this.state;

        if (fechaI.estatus && fechaT.estatus && puesto.estatus) {
            if (editar.editar) {
                let newArray = [...dataSource];
                let newArraySend = [...dataSourceSend];
                console.log("editar " + editar.id);
                let temp = dataSource[editar.id ];
                console.log("se modifico ", temp);
                let edit = {
                    key: temp.key,
                    fechaIncio: fechaI.value,
                    fechaTermino: fechaT.value,
                    rama: rama.name,
                    puesto: puesto.name,
                    instituto: instituto.name,
                    entidad: entidad.name,
                    distrito: distrito.name
                };
                let editSend = {
                    key: temp.key,
                    fechaIncio: fechaI.value,
                    fechaTermino: fechaT.value,
                    rama: rama.value,
                    puesto: puesto.value,
                    instituto: instituto.value,
                    entidad: entidad.value,
                    distrito: distrito.value
                };
                console.log("el arreglo", edit);
                newArray[editar.id ] = edit;
                setDataSource(newArray);
                newArraySend[editar.id ] = editSend;
                setDataSourceSend(newArraySend);

                setFechaI({ estatus: false, value:null, name: "" });
                setFechaT({ estatus: false, value:null, name: "" });
                setRama({ estatus: false, value:null, name: "" });
                setPuesto({ estatus: false, value:null, name: "" });
                setInstituto({ estatus: false, value:null, name: "" });
                setEntidad({ estatus: false, value:null, name: "" });
                setDistrito({ estatus: false, value:null, name: "" });
                setEditar({ editar: false, id: null });

            } else {
                const newData = {
                    key: count,
                    fechaIncio: fechaI.value,
                    fechaTermino: fechaT.value,
                    rama: rama.name,
                    puesto: puesto.name,
                    instituto: instituto.name,
                    entidad: entidad.name,
                    distrito: distrito.name
                };
                const newDataSend = {
                    key: count,
                    fechaIncio: fechaI.value,
                    fechaTermino: fechaT.value,
                    rama: (rama.value != null?rama.value:null),
                    puesto: (puesto.value != null?puesto.value:null),
                    instituto: (instituto.value != null?instituto.value:null),
                    entidad: (entidad.value != null?entidad.value:null),
                    distrito: (distrito.value != null?distrito.value:null),
                };
                // se actuliza el estado de un arreglo
                setFechaI({ estatus: false, value: null, name: "" });
                setFechaT({ estatus: false, value: null, name: "" });
                setRama({ estatus: false, value: null, name: "" });
                setPuesto({ estatus: false, value: null, name: "" });
                setInstituto({ estatus: false, value: null, name: "" });
                setEntidad({ estatus: false, value: null, name: "" });
                setDistrito({ estatus: false, value: null, name: "" });
                setDataSourceSend(dataSourceSend.concat(newDataSend));
                setDataSource(dataSource.concat(newData));
                console.log(newDataSend)
                let a = count;
                setCount(++a);

            }
        } else {
            alertINE("error", "Debes llenar todos los campos.");
        }
    };

    const changeFechaInicio = (data, dateString) => {
        //console.log("fecha inicio ", dateString);
        //console.log("fecha inicio ", dateString.length);
        if (dateString.length) {
            setFechaI({ estatus: true, value: dateString });
        }
    };
    const changeFechaFinal = (data, dateString) => {
        //console.log("fecha final", dateString);
        if (dateString.length) {
            setFechaT({ estatus: true, value: dateString });
        }
    };

    const changeRama = e => {
        //console.log("rama", e.target);
        setRama({ estatus: true, value: e.target.value, name: e.target.id });
    };

    const changePuestoExperiencia = (value, e) => {
        //console.log("puesto ", e.props.children);
        setPuesto({ estatus: true, value: e.props.value, name: e.props.children });
    };

    const changeInstituto = e => {
        //console.log("Instituto", e.target.value);
        setInstituto({ estatus: true, value: e.target.value, name: e.target.id });
    };

    const changeEntidadFederativa = (value, e) => {
        //console.log("entidad Federativ ", e);
        setLlenarDistrito(e.props.value)
        setDistrito({ estatus: false, value: null, name: "" });
        setEntidad({ estatus: true, value: e.props.value, name: e.props.children });
    };
    const changeDistrito = (value, e) => {
        //console.log("distrito ", e);
        setDistrito({ estatus: true, value: e.props.value, name: e.props.children });
    };
    return (
        <>
            <div >
                <div className="row">
                    <div className="col-md-3 col-sm-4">
                        <font color="#d5007f">*</font>Fecha de inicio
            <br />
                        <DatePicker
                            placeholder={"DD/MM/AAAA"}
                            format={"DD/MM/YYYY"}
                            value={fechaI.estatus ? moment(fechaI.value, "DD-MM-YYYY") : ""}
                            onChange={(evento, fecha) => changeFechaInicio(evento, fecha)}
                        />
                    </div>
                    <div className="col-md-3 col-sm-4">
                        <font color="#d5007f">*</font>Fecha de término
            <br />
                        <DatePicker
                            placeholder={"DD/MM/AAAA"}
                            format={"DD/MM/YYYY"}
                            value={fechaT.estatus ? moment(fechaT.value, "DD-MM-YYYY") : ""}
                            onChange={(evento, fecha) => changeFechaFinal(evento, fecha)}
                        />
                    </div>
                    <div className="col-md-6 col-sm-4">
                        Rama
            <br />
                        <Radio.Group
                            onChange={changeRama}
                            value={rama.estatus ? rama.value : ""}
                        >
                            <Radio value={1} id="Administrativo">
                                Administrativo
              </Radio>
                            <Radio value={2} id="Servicio Profesional">
                                Servicio Profesional
              </Radio>
                        </Radio.Group>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 col-sm-4">
                        <font color="#d5007f">*</font>Puesto
            <br />
                        <Select
                            onChange={changePuestoExperiencia}
                            value={puesto.estatus ? puesto.value : "Selecciona una opción"}
                            placeholder="Selecciona una opción"
                            style={{ width: '100%' }}
                        >
                            {
                                puestos != null ?
                                    puestos.map(tem =>
                                        <Select.Option value={tem.idPuesto}>{tem.descripcion}</Select.Option>
                                    )
                                    : null

                            }
                        </Select>

                    </div>
                    <div className="col-md-4 col-sm-4">
                        Instituto
            <br />
                        <Radio.Group
                            onChange={changeInstituto}
                            value={instituto.estatus ? instituto.value : ""}
                        >
                            <Radio value={1} id="INE">
                                INE
              </Radio>
                            <Radio value={2} id="OPL">
                                OPL
              </Radio>
                        </Radio.Group>
                    </div>
                    <div className="col-md-4 col-sm-4">
                        Entidad Federativa
                        
                        
            <br />
                        <Select
                            onChange={changeEntidadFederativa}
                            value={entidad.estatus ? entidad.value : "Selecciona una opción"}
                            placeholder="Selecciona una opción"
                            style={{ width: '100%' }}
                            
                        >
                            {
                                Estados != null ?
                                    Estados.estados.map((tem) => (
                                        <Select.Option value={tem.idEstado}>{tem.nombreEstado}</Select.Option>
                                    ))
                                    : null

                            }
                        </Select>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-5 col-sm-4">
                        Distrito
            <br />
                        <Select
                            disabled={!entidad.estatus}
                            onChange={changeDistrito}
                            placeholder="Selecciona una opción"
                            value={
                                distrito.estatus ? distrito.value : "Selecciona una opción"
                            }
                            style={{ width: '100%' }}
                        >
                            {
                                llenarDistrito != null ?
                                    Estados.estados[(llenarDistrito - 1)].distritos.map((tem) => (
                                        <Select.Option value={tem.idDistrito}>{tem.nombreDistrito}</Select.Option>
                                    ))
                                    : 
                                editar.editar &&  entidad.value !=null?
                                
                                    //  <Select.Option value={2}>{JSON.stringify(entidad.value)}</Select.Option>
                                    
                                
                                    Estados.estados[(entidad.value)].distritos.map((tem) => (
                                        <Select.Option value={tem.idDistrito}>{tem.nombreDistrito}</Select.Option>
                                    ))
                                    : null
                            }
                           
                        </Select>
                        <Button
                            onClick={handleAdd}
                            type="primary"
                            icon={<DownloadOutlined />}
                            style={{
                                marginLeft: 16
                            }}
                        >
                            {editar.editar ? "Guardar" : "Agregar"}
                        </Button>
                    </div>
                </div>
            </div>
            <br />


            <Table locale={ {emptyText: 'No hay registros' }}  dataSource={dataSource} columns={columns} />
        </>
    );
}


