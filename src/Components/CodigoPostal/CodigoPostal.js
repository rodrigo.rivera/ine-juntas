import { url_codigoPostal } from "../../Configurations/Urls";
import axios from "axios";

/**
 * regresa un json con los datos de el api externa
 * @param {*} cp  codigo postal longitud minima  5  y maxima 6
 */
export function getCodigoPolstal(codigoPostal) {
  //console.log("url codigo ",url_codigoPostal)
  return new Promise((resolve, reject) => {
    axios
      .get(`${url_codigoPostal}/${codigoPostal}`)
      .then((response) => {
        const respuesta = {
          error: true,
          pais: null,
          entidad: null,
          municipio: null,
          codigo: codigoPostal,
          colonias: null,
        };
        //console.log(response.data[0].response);
        if (response.status === 200) {
          respuesta.error = false;
          respuesta.pais = response.data[0].response.pais;
          respuesta.entidad = response.data[0].response.estado;
          respuesta.municipio = response.data[0].response.municipio;
          //respuesta.codigo = response.data[0].response.cp;
          respuesta.colonias = getColnias(response.data);
          //console.log("respuetas ", respuesta)
          resolve(respuesta);
        } else {
          resolve(respuesta);
        }
      })
      .catch((err) => {
        console.log("erorr C.P: ", err);
        resolve({
          error: true,
          pais: null,
          entidad: "",
          municipio: "",
          codigo: codigoPostal,
          colonias: null,
        });
      });
  });
}

/**
 * llena  arreglo con el  nombre de las colonias
 * @param {*} array
 */
function getColnias(array) {
  const arregloColonias = [];
  array.map((temp) => {
    //console.log(temp.response.asentamiento)
    arregloColonias.push(temp.response.asentamiento);
  });
  return arregloColonias;
}
