import React, { useState, useEffect } from 'react'
//import axios from 'axios';
import "./Proceso_Menu.scss";
import { Popover, Select, Icon, Button } from "antd";
import { getMenu } from "../../ActionsService/HomeActionService";
import { Dialog, DialogContent, DialogActions } from "@material-ui/core";
import { useHistory } from "react-router-dom";
// import { basePath } from "../../Configurations/Urls"
// import { storeMenu } from '../../Reducers/HomeRed';

import { storeMenu } from "../../Reducers/HomeRed";
import { createStore } from 'redux';

const { Option } = Select;
// const store = createStore(storeMenu)

//console.log(dataMenu);

export default function ProcessMenu(props) {
    const history = useHistory();
    let { dataMenu, store, storeLoad, isLoadingMainMenu } = props;

    // console.log("procesMenu ", props)
    // console.log("tempra l ", (dataMenu.infoMenu.listaEstados == undefined),
    //     (dataMenu.infoMenu.listaDistritosFed == undefined))

    //VALIDACION DE ROLES
    const isRolJD = dataMenu.rolUsuario.includes('.JD') ? true : false;
    const isRolJL = dataMenu.rolUsuario.includes('.JL') ? true : false;
    const isRolOC = dataMenu.rolUsuario.includes('.OC') ? true : false;

    /**
    * Estados 
    */
    const [showPopOver, setShowPopOver] = useState(false)
    const [disableDistrito, setDisableDistrito] = useState(true)
    const [openDialog, setOpenDialog] = useState(false);
    const [dataEnviar, setEnviar] = useState(null)
    const [proceso, setProceso] = useState(null);
    const [entidad, setEntidad] = useState(null);
    const [distrito, setDistrito] = useState(null);
    //const [listaProeceso] = useState(dataMenu.infoMenu.listaDetalles);
    const [listaEntidad, setListaEntidad] = useState(dataMenu.infoMenu.listaEstados == undefined ? null : dataMenu.infoMenu.listaEstados);
    const [listaDistrito, setListaDistrito] = useState(dataMenu.infoMenu.listaDistritosFed == undefined ? null : dataMenu.infoMenu.listaDistritosFed);

    const handleAceptar = () => {
        setOpenDialog(false)
        history.goBack()
    };

    const handleCancelar = () => {
        setOpenDialog(false)
    };
    
    /**
     * estado Redux
     */
    storeLoad.subscribe(() => {
        let respuestaStore = storeLoad.getState();
        if (respuestaStore.pending) {
            setDisableDistrito(true)
        }
        else {
            setDisableDistrito(false)
        }
    }

    )
    store.subscribe(() => {
        let respuestaStore = store.getState();
        if (localStorage.getItem("waitingResponse")) {
            setDisableDistrito(true)
        } else {
            setDisableDistrito(false)
        }
        if (respuestaStore.isLoading) {
            setDisableDistrito(true)
            localStorage.setItem("waitingResponse", true);
        }
        if (!respuestaStore.isLoading) {
            setEnviar(respuestaStore.menu)
            //console.log("esta completo", respuestaStore.menu)
            setListaEntidad(respuestaStore.menu.infoMenu.listaEstados)
            setListaDistrito(auxGetDistrito(respuestaStore.menu.infoMenu));
            setDisableDistrito(false)
            localStorage.setItem("waitingResponse", false);
        }
    });

    /**
     * 
     * @param {*} data 
     */
    const auxGetDistrito = (data) => {
        if (data.listaDistritosFed != null) {
            return data.listaDistritosFed;
        }
        if (data.listaDistritosLoc != null) {
            return data.listaDistritosLoc;
        }
        if (data.listaMunicipios != null) {
            return data.listaMunicipios;
        }
        return null;
    }

    /**
     * 
     * @param {*} value 
     */
    const changeProceso = value => {
        //console.log("Cambiaste el proceso " + value);        
        setProceso(dataMenu.infoMenu.listaDetalles[value]);
        dataMenu.infoMenu.detalleSelec = dataMenu.infoMenu.listaDetalles[value]
        dataMenu.tipoCambioGeografico = 1
        getMenu(dataMenu, store);
    }

    /**
     * 
     * @param {*} value 
     */
    const ChangeEndidad = value => {

        setDistrito(null);
        
        dataMenu.infoMenu.estadoSelec = dataMenu.infoMenu.listaEstados[parseInt(value)];
        dataMenu.tipoCambioGeografico = 2
        
        setEntidad(dataMenu.infoMenu.estadoSelec);

        store.dispatch({ type: "MENU_LOADING" });

        getMenu(dataMenu)
        .then((response) => {

            if (!response.error) {
                store.dispatch({ type: "MENU_LOADED", menu: response.data });
                console.log("TERMINE LA PETICION-GETMENU-ENTIDAD");
                console.log(response);
                
            }else {
                store.dispatch({ type: "MENU_LOADED", menu: [] });
                console.log("TERMINE LA PETICION-GETMENU: ERROR " + response.error);
                alert("Ocurrió un error al obtener los distritos, intente más tarde.");
                window.location.href = localStorage.getItem("consejo");
            }
        
        });
        
        if(value === '0')
            setShowPopOver(false)
    }
    /**
     * 
     * @param {*} value 
     */
    const ChangeDistrito = value => {

        dataMenu = dataEnviar

        if(dataMenu.infoMenu.distritoFedSelec != null) {

            if(parseInt(value) != dataMenu.infoMenu.distritoFedSelec.idDistrito) {

                store.dispatch({ type: "MENU_LOADING" });

                let temData = auxSeleccionarDitrito(dataMenu, value);
                setDistrito(temData[1])
                dataMenu = temData[0];
                dataMenu.tipoCambioGeografico = 3;

                store.dispatch({ type: "MENU_LOADED", menu: dataMenu });
                console.log("TERMINE LA PETICION-GETMENU-DISTRITO-1");
                console.log(dataMenu);

                setOpenDialog(false)
                history.push("/");
            }
        } else {

            store.dispatch({ type: "MENU_LOADING" });

            let temData = auxSeleccionarDitrito(dataMenu, value);
            setDistrito(temData[1])
            dataMenu = temData[0];
            dataMenu.tipoCambioGeografico = 3

            getMenu(dataMenu)
            .then((response) => {
    
            if (!response.error) {
              store.dispatch({ type: "MENU_LOADED", menu: response.data });
              console.log("TERMINE LA PETICION-GETMENU-DISTRITO-2");
              console.log(response);
            }else {
              store.dispatch({ type: "MENU_LOADED", menu: [] });
              console.log("TERMINE LA PETICION-GETMENU: ERROR " + response.error);
              alert("Ocurrió un error al obtener los distritos, intente más tarde.");
              window.location.href = localStorage.getItem("consejo");
            }
          });
        }

        setShowPopOver(false)
    }

    /**
     * 
     * @param {*} data 
     * @param {*} value 
     */
    const auxSeleccionarDitrito = (data, value) => {
        if (data.infoMenu.listaDistritosFed != null) {
            data.infoMenu.distritoFedSelec = data.infoMenu.listaDistritosFed[value]
            return [data, data.infoMenu.listaDistritosFed[value]];
        }
        if (data.infoMenu.listaDistritosLoc != null) {
            data.infoMenu.distritoFedSelec = data.infoMenu.listaDistritosLoc[value]
            return [data, data.infoMenu.listaDistritosLoc[value]];
        }
        if (data.infoMenu.listaMunicipios != null) {
            data.infoMenu.municipioSelec = data.infoMenu.listaMunicipios[value];
            return [data, data.infoMenu.listaMunicipios[value]];
        }
        setShowPopOver(false)
        return null;
    }

    useEffect(() => {
        if(!isLoadingMainMenu) {
          if(isRolJD) {
            console.log("dataEnviar");
            console.log(dataMenu);
            //ENTIDAD
            setEntidad(dataMenu.infoMenu.estadoSelec);
            //DISTRITO
            setDistrito(dataMenu.infoMenu.distritoFedSelec);
          }
          if(isRolJL) {
            setDisableDistrito(false);
            setEnviar(dataMenu);
            //ENTIDAD
            setEntidad(dataMenu.infoMenu.estadoSelec);
            setListaDistrito(auxGetDistrito(dataMenu.infoMenu));
          }
        }
        
      }, []);

    const contenidoMenuUsuario = () => {
        return (
            <div>
                {/* <div className="etiqueta">Proceso Electoral</div>
                <Select
                    //defaultValue="Selecciona"
                    style={{ width: 280 }}
                    //onChange={handleChange}
                    value={proceso != null ? proceso.descripcion : "Selecciona"}
                    disabled={listaProeceso != null ? false : true}
                    onChange={changeProceso}
                >
                    {listaProeceso !=null ?listaProeceso.map((detalle, id) => (
                        <Option key={id}>{detalle.descripcion}</Option>
                    )):null}
                </Select>
                <p> </p> */}
                {/* 
                    Datos de Entidad 
                    */}
                <div className="etiqueta">Entidad</div>
                <Select
                    //defaultValue="Selecciona"
                    // notFoundContent="No hay Entidades registradas."
                    value={entidad != null ? entidad.nombreEstado : "Selecciona"}
                    style={{ width: 280 }}
                    disabled={isRolJL || (listaEntidad != null ? false : true)}
                    onChange={ChangeEndidad}
                >
                    {listaEntidad != null &&
                        listaEntidad.map((estado, i) => {
                            return (<Option key={i}>{estado.nombreEstado}</Option>)
                        })
                    }
                </Select>
                <p> </p>
                {/* 
                Datos de Distito  
                */}
                <div className="etiqueta">Distrito</div>
                <Select
                    defaultValue="Selecciona"
                    // notFoundContent="No hay Distritos registrados para la Entidad Seleccionada."
                    value={distrito != null ? distrito.nombreDistrito : "Selecciona"}
                    style={{ width: 280 }}
                    disabled={(!disableDistrito && listaDistrito != null) ? false : true}
                    onChange={ChangeDistrito}
                >
                    {listaDistrito != null &&
                        listaDistrito.map((tempDistito, i) => {
                            let nombreDistrito = tempDistito.nombreDistrito === null ? 'JUNTA LOCAL' : tempDistito.nombreDistrito
                            return (<Option key={i}>{tempDistito.idDistrito +" - "+ nombreDistrito }</Option>)
                        })
                    }
                </Select>
                
            </div>

        );
    }

    const click = () => {
        let actualPath = window.location.pathname;

        if (!openDialog) {
            if (actualPath.includes("editar") | actualPath.includes("crear")) {
                setOpenDialog(true)
            } else {
                setShowPopOver(true)
                setOpenDialog(false)
            }
        }
        showPopOver === true ? setShowPopOver(false) : setShowPopOver(true);
    };

    return (
        <div>
            {
                isRolJD === true ?
                <p className="proceso">
                    {
                    entidad && (
                    <>
                        <Icon type="environment" theme="filled" /> {entidad.nombreEstado}{" "}
                        {distrito === null ? '' : distrito.idDistrito === 0 ? 'JUNTA LOCAL' : distrito.nombreDistrito}
                    </>
                    )}
                </p>
                
                :

                <Popover visible={showPopOver} mouseLeaveDelay={0.2} content={contenidoMenuUsuario()} trigger="click" onClick={click}>
                    <div className="proceso">
                        {isRolOC === true ? "Oficinas Centrales" : ""}
                        <br/>
                        {entidad &&
                            <>
                                <Icon type="environment" theme="filled" /> {entidad.nombreEstado} - {distrito === null ? '' : distrito.idDistrito === 0 ? 'JUNTA LOCAL' : distrito.nombreDistrito}
                            </>
                        }
                        <Dialog open={openDialog}>
                        <DialogContent>Si cambias de distrito se eliminarán tus cambios</DialogContent>
                        <DialogActions>
                            <Button type="primary" onClick={handleAceptar}>Aceptar</Button>
                            <Button type="secondary" onClick={handleCancelar}>Cancelar</Button>
                        </DialogActions>
                    </Dialog>
                    </div>
                    
                </Popover>
            }
        </div>
        
    )
}

