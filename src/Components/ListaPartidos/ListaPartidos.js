import React, { useState } from "react";
import '../../../node_modules/antd/dist/antd.css';
import "../../Assets/css/index.css";
//import '../../Pages/index.css'
import { Button } from '../../../node_modules/antd';
import { FormOutlined } from '../../../node_modules/@ant-design/icons';
import { List, Avatar } from '../../../node_modules/antd';
import { Pagination } from '../../../node_modules/antd';
import {
    Link,
    Redirect
} from 'react-router-dom';

import PAN from '../../Assets/partidos/pan.png'
import PRI from '../../Assets/partidos/pri.png'
import PRD from'../../Assets/partidos/prd.png'
import PVEM from'../../Assets/partidos/partido_verde.png'
import MORENA from '../../Assets/partidos/morena.png'
import PT from '../../Assets/partidos/partido_trabajo.png'
import MOVIMIENTO_CIUDADANO from '../../Assets/partidos/movimiento_ciudadano2019.png'
import defaultLogo from '../../Assets/partidos/CandidatoIndependiente.svg'
const dictPartidos = {
    1:PAN,
    2:PRI,
    3:PRD,
    5:PVEM,
    8:MORENA,
    4:PT,
    6:MOVIMIENTO_CIUDADANO
}

const logosPartidos = { 
    PAN:'../../Assets/partidos/encuentro_social.png', 
    PRI:'../../Assets/partidos/pri.png', 
    PRD:'../../Assets/partidos/prd.png', 
    PVEM:'../../Assets/partidos/partido_verde.png',
    MORENA:'../../Assets/partidos/morena.png',
    PT:'../../Assets/partidos/partido_trabajo.png',
    MOVIMIENTO_CIUDADANO:'../../Assets/partidos/movimiento_ciudadano2019.png' };

export default function ListaPartidos(props) {

    var partidos = props.partidos
    const [totalPaginas] = useState(props.totalPaginas)
    const [paginaActual, setPaginaActual] = useState(1)
    const [listaActualPartidos, setListaActualPartidos] = useState(props.listaActualPartidos)
    const temp = JSON.parse(localStorage.getItem("menu"))
    const [botonEditar] = useState(temp[0].subMenus[0].modulos[0].listAccionesModulo)


    const onClick = e => {
        console.log('click')
    }
    const onChange = page => {
        console.log('cambio de pagina ' + page)
        setPaginaActual(page)
        console.log(partidos)
        setListaActualPartidos(divideListaPartidos(partidos, page))
    }
    const divideListaPartidos = (partidos, page) => {
        const fin = (page * 5) > partidos.lenght ? partidos.lenght : (page * 5)
        const inicio = fin - 5
        var indice;
        var i = 0
        var array = []
        for (indice = inicio; indice < fin; indice++ , i++) {
            array[i] = partidos[indice]
        };
        return array.filter(Boolean)
    }




    return (
        <div className="card mb-3" >
            <div className="row no-gutters">
                {/* <p>lista{paginaActual}</p> */}
                <List
                    itemLayout="horizontal"
                    dataSource={listaActualPartidos}
                    size='large'
                    renderItem={item => (
                        <List.Item key={item.idAsociacion}>
                            {/* {console.log('id ********** '+JSON.stringify(item))} */}
                            <List.Item.Meta
                                avatar={<Avatar shape="square" size={64} src={item.idAsociacion in dictPartidos?dictPartidos[item.idAsociacion]:defaultLogo} />}
                                title={<a className={item.acreditacion==0?'ant-list-item-meta-title-disabled':''} href={item.idDomicilioAsociacion+'/editar'}
                                    
                                    >{item.nombreAsociacion}</a>}//item.nombreAsociacion
                                
                                description={
                                    <div className={'additional '+item.acreditacion==0?'ant-list-item-meta-description-disabled':''}>
                                        <pListaPartidos><strong>Dirección: </strong> {item.calle}  {item.numeroExterior}  {item.colonia}  C.P. {item.codigoPostal} {item.idEstado}  {item.idMunicipio}</pListaPartidos>
                                        <br/>
                                        <pListaPartidos><strong>Correo: </strong> {item.correoNotificaciones}</pListaPartidos>
                                        <br/>
                                        <pListaPartidos><strong>Teléfono: </strong> {item.telefono}</pListaPartidos>
                                    </div>}
                                    
                            />
                            {/* {console.log('id :::: '+item.idAsociacion)} */}
                            {/* <Button className='.ant-btn ant-btn-modifica' onClick={onClick}> */}
                            <div style={{paddingLeft:'50px'}}>
                                {/* <FormOutlined style={{ marginLeft: '50px' }} size={35} /> */}
                                {botonEditar.map((temp) =>
                                    temp.idAccion === 3 && (
                                        <Link to={{
                                            pathname: `${item.idDomicilioAsociacion}/editar`,
                                        }
                                        } >
                                        <Button key={JSON.stringify(temp)}  shape="circle" className='.ant-btn ant-btn-modifica'>
                                            <FormOutlined size={35} />
                                            {/* {console.log("que es item",item)} */}
                                             
                                        </Button>
                                        </Link>
                                    )
                                )}
                            </div>
                            {/* </Button> */}
                        </List.Item>
                    )}
                />
            </div>
            <Pagination current={paginaActual} total={totalPaginas * 10} onChange={onChange} style={{ textAlign: 'center' }} />
        </div>
    )
}