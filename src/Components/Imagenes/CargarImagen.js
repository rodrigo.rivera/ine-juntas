import React, { useState } from 'react';
import 'antd/dist/antd.css';
//import './index.css';
import { Upload, message, Tooltip } from 'antd';
import alertINE from "../../Components/Alert/AlertsINE";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        alertINE("error", "Solo soporta archvis jpg y png");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        alertINE("error", "El tamaño de la imagene debe de ser menor a 2MB!");
    }
    return isJpgOrPng && isLt2M;
}

export default function CargarImgen(props) {

    const { getData, ponerImagen, texto, editar } = props;
    const [loading, setLoading] = useState(null);
    const [imageUrl, setImageUrl] = useState(null);

    const handleChange = info => {
        if (info.file.status === 'uploading') {
            //this.setState({ loading: true });
            setLoading(true)
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            //console.log(info.file)
            getData(info.file)
            getBase64(info.file.originFileObj, imageUrl => {
                setImageUrl(imageUrl)

                setLoading(false)
            }

            );
        }
    };

    const mostrartImagen = () => {
        //console.log(ponerImagen)
        getBase64(ponerImagen.originFileObj, imageUrl => {
            setImageUrl(imageUrl)
        });
    }

    return (
        <div className="row" >


            <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                beforeUpload={beforeUpload}
                onChange={handleChange}
            >

                {ponerImagen ?
                    <>
                        {mostrartImagen()}
                        <img src={imageUrl} alt="avatar" style={{ width: '100%' }} />
                    </>
                    :

                    editar != undefined && editar != null ?
                        <img src={editar} alt="avatar" style={{ width: '100%' }} />
                        :

                        <div>
                            <Tooltip placement="right" title="Poner Texto ">
                                <i className="cargarImg" />
                            </Tooltip>
                            {loading ? <LoadingOutlined /> : <PlusOutlined />}
                            <div className="ant-upload-text">{texto}</div>

                        </div>
                }

            </Upload>

        </div >
    );

}

