import React, { useState } from "react";
import { Textbox } from "react-inputs-validation";
import "./inputINE.css";
import { CloseCircleFilled, CheckCircleFilled } from '@ant-design/icons'
import './bootstrap_white.css';
import ReactTooltip from 'react-tooltip'
TextField.defaultProps = {
    tooltipPosition: 'right',
    placeHolder: 'Ingresa tus datos',
    type: 'text',
    tooltipMessage: '',
    paddingLeft: '10px',
    disabled: false,
    value: ''
};
// const styles = {
//     display: 'table-cell',
//     height: '60px',
//     width: '80px',
//     textAlign: 'center',
//     background: 'white',
//     verticalAlign: 'middle',
//     border: '5px solid white',
// };
export default function TextField(props) {
    const { placeHolder, id, name, type, regex, label, icon, warningMessage, tooltipMessage, tooltipPosition, paddingLeft, disabled, value } = props
    const [className, setClassName] = useState('')
    const [validationIcon, setValidationICon] = useState('')

    const handleLangChange = (content) => {

        props.onSelectLanguage(content, id, name, regex, warningMessage, disabled);
        const reg = regex
        // const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (reg.test(String(content).toLowerCase())) {
            //console.log(reg.test(String(content).toLowerCase()))
            setClassName(props.disabled ? 'disabled' : 'correcto')
            setValidationICon(<CheckCircleFilled style={{ color: '#389b73' }} />)
        } else {
            //console.log(reg.test(String(content).toLowerCase()))
            setClassName(props.disabled ? 'disabled' : 'incorrecto')
            setValidationICon(<CloseCircleFilled style={{ color: '#79134c' }} />)
        }
    }

    return (
        // <Tooltip placement={tooltipPosition}
        //     option={{ backgroundColor: 'red' }}
        //     style={{ backgroundColor: 'red' }}
        //     type='success'
        //     data-background-color='red'
        //     backgroundColor='red'
        //     overlay='ejemploasdasd'
        //     arrowColor='green'
        //     arrowContent={<div className="rc-tooltip-arrow-inner"></div>}
        // >

        <div style={{ position: "relative" }}>
            {label}
            <div
                style={{
                    position: "absolute",
                    left: "0",
                    top: "69%",
                    zIndex: "1",
                    transform: "translateY(-70%)",
                    color: "#666",
                    paddingLeft: '10px'
                }}
            >
                {icon}
            </div>


            <p data-tip={tooltipMessage}><Textbox
                value={value}
                attributesInput={{
                    id: id,
                    name: name,
                    placeholder: placeHolder,
                    type: type,
                    className: "ant-input " + className + " ant-input-affix-wrapper ant-input-affix-wrapper-lg"

                }}
                onChange={(textInput, e) => {
                    //console.log(textInput)
                    handleLangChange(textInput)
                }} //Required.[Func].Default: () => {}. Will return the value.
                onBlur={e => { }} //Optional.[Func].Default: none. In order to validate the value on blur, you MUST provide a function, even if it is an empty function. Missing this, the validation on blur will not work.
                validationOption={{
                    name: "textInput", //Optional.[String].Default: "". To display in the Error message. i.e Please enter your {name}.
                    check: true, //Optional.[Bool].Default: true. To determin if you need to validate.
                    required: false, //Optional.[Bool].Default: true. To determin if it is a required field.
                    customFunc: textInput => {
                        const reg = regex
                        // const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        if (reg.test(String(textInput).toLowerCase())) {
                            setClassName(props.disabled ? 'disabled' : 'correcto')

                            return true;
                        } else {
                            setClassName(props.disabled ? 'disabled' : 'incorrecto')
                            setValidationICon(<CloseCircleFilled style={{ color: '#d5007f' }} />)
                            return false;
                        }
                    }
                }}

            /></p>
            <ReactTooltip place={tooltipPosition} effect='solid' />
            {/* <CheckCircleFilled /> 
            <CloseCircleFilled />
            */}
            <div className='disable'
                style={{
                    position: "absolute",
                    right: "10px",
                    top: "69%",
                    visibility: "showed",
                    zIndex: "1",
                    transform: "translateY(-70%)",
                    color: "#666",
                    paddingLeft: '10px'
                }}
            >
                {validationIcon}
            </div>


        </div>
        // </Tooltip>

    )
}