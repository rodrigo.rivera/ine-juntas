import React, { useState } from "react";
import { Textbox } from "react-inputs-validation";
import "./inputINE.css";
import { CloseCircleFilled, CheckCircleFilled } from "@ant-design/icons";
// import "./bootstrap_white.css";
import ReactTooltip from "react-tooltip";
TextField.defaultProps = {
  tooltipPosition: "right",
  placeHolder: "",
  type: "text",
  tooltipMessage: "",
  paddingLeft: "10px",
  value: "",
};
// const styles = {
//     display: 'table-cell',
//     height: '60px',
//     width: '80px',
//     textAlign: 'center',
//     background: 'white',
//     verticalAlign: 'middle',
//     border: '5px solid white',
// };
export default function TextField(props) {
  const {
    placeHolder,
    id,
    name,
    type,
    regex,
    label,
    icon,
    warningMessage,
    tooltipMessage,
    tooltipPosition,
    value,
    disabled,
    number
  } = props;
  const [maxLength] = useState(props.maxLength > 0 ? props.maxLength : 50)

  const [className, setClassName] = useState("");
  const [validationIcon, setValidationICon] = useState("");

  const handleLangChange = (content) => {

    // if (content.length > 3) {
    //   console.log("BORRAR::::::::::::::::::")
    //   setValue(content.substring(0, 3));
    //   content = content.substring(0, 3);
    //   console.log(value + "::" + value.length + "-----" + content + " length " + content.length)
    // }
    content = content.trim();
    props.onSelectLanguage(content, id, name, regex, warningMessage);
    const reg = regex;
    // const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


    if (content === "null" | content === null | content === undefined) {
      content = ""
    }
    if (content === " ") {
      setClassName("incorrecto");
      setValidationICon(<CloseCircleFilled style={{ color: "#79134c" }} />);
    } else
      if (reg.test(String(content).toLowerCase())) {
        //console.log(reg.test(String(content).toLowerCase()));
        setClassName("correcto");
        setValidationICon(<CheckCircleFilled style={{ color: "#389b73" }} />);
      } else {
        //console.log(reg.test(String(content).toLowerCase()));
        setClassName("incorrecto");
        setValidationICon(<CloseCircleFilled style={{ color: "#79134c" }} />);
      }
  };
  const convierteNo = (numero) => {
    let res = 'hola'
    if (numero === '-1' | numero === 'null' | numero === undefined | numero === null) {
      res = ''
    }
    else {
      res = numero
    }
    return res
  }

  return (
    // <Tooltip placement={tooltipPosition}
    //     option={{ backgroundColor: 'red' }}
    //     style={{ backgroundColor: 'red' }}
    //     type='success'
    //     data-background-color='red'
    //     backgroundColor='red'
    //     overlay='ejemploasdasd'
    //     arrowColor='green'
    //     arrowContent={<div className="rc-tooltip-arrow-inner"></div>}
    // >

    <div style={{ position: "relative" }}>
      {label}
      <div
        style={{
          position: "absolute",
          left: "0",
          top: "59%",
          zIndex: "1",
          transform: "translateY(-60%)",
          color: "#666",
          paddingLeft: "10px",
        }}
      >
        {icon}
      </div>

      <div data-tip={tooltipMessage}>
        <Textbox
          disabled={disabled}
          value={convierteNo(value)}
          attributesInput={{
            maxLength: maxLength,
            id: id,
            name: name,
            placeholder: placeHolder,
            type: type,
            className:
              "ant-input " +
              className +
              " ant-input-affix-wrapper ant-input-affix-wrapper-lg",
          }}
          onChange={(textInput, e) => {
            //console.log(textInput);
            handleLangChange(textInput);
          }} //Required.[Func].Default: () => {}. Will return the value.
          onBlur={(e) => { }} //Optional.[Func].Default: none. In order to validate the value on blur, you MUST provide a function, even if it is an empty function. Missing this, the validation on blur will not work.
          validationOption={{
            type: number,
            numberType: 'int',
            name: "textInput", //Optional.[String].Default: "". To display in the Error message. i.e Please enter your {name}.
            check: true, //Optional.[Bool].Default: true. To determin if you need to validate.
            required: false, //Optional.[Bool].Default: true. To determin if it is a required field.
            // customFunc: (textInput) => {
            //   const reg = regex;
            //   // const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            //   if (reg.test(String(textInput).toLowerCase())) {
            //     setClassName("correcto");

            //     return true;
            //   } else {
            //     textInput = ""
            //     setClassName("incorrecto");
            //     setValidationICon(
            //       <CloseCircleFilled style={{ color: "#d5007f" }} />
            //     );
            //     return false;
            //   }
            // },
          }}
        />
      </div>
      <ReactTooltip place={tooltipPosition} effect="solid" />
      {/* <CheckCircleFilled /> 
            <CloseCircleFilled />
            */}
      <div
        className="disable"
        style={{
          position: "absolute",
          right: "10px",
          top: "55%",
          visibility: "showed",
          zIndex: "1",
          transform: "translateY(-70%)",
          color: "#666",
          paddingLeft: "10px",
        }}
      >
        {validationIcon}
      </div>
    </div>
    // </Tooltip>
  );
}
