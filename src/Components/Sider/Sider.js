import React, { useState } from "react";
import { Layout, Menu, Button, Divider } from "antd";
import "./Sider.scss";
import { Link } from "react-router-dom";
import { BarChartOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { Dialog, DialogContent, DialogActions } from "@material-ui/core";
import { fetchLogout } from "../../ActionsService/HomeActionService";
import { HomeOutlined } from '@ant-design/icons';
import "antd/dist/antd.css";
import Icon from '@ant-design/icons';

export default function Sider(props) {

  let {store} = props;
  const { SubMenu } = Menu;
  const { Sider } = Layout;
  const [menu, setMenu] = useState(props.menu);
  const [alertOpen, setAlertOpen] = useState(false);
  const [collapsed, setCollapsed] = useState(true);
  const [selectedKeysMenu, setSelectedKeysMenu] = useState([]);

  /**
  * estado Redux
  */
  store.subscribe(() => {
    
    let respuestaStore = store.getState();

    if (!respuestaStore.isLoading) {
      setMenu(respuestaStore.menu.infoMenu.listMenu);
      setSelectedKeysMenu([]);
    }
  });

  const handleClickOpen = () => {
    setAlertOpen(true);
  };

  const handleClose = () => {
    setAlertOpen(false);
  };

  const logout = () => {
    setAlertOpen(false);
    fetchLogout(props.accessToken)(null);
  };

  console.log("sider-menu", props)

  const onClick = e => {
    const aplicaciones = localStorage.getItem('aplicaciones')
    window.location.href = aplicaciones + "/sesiones/aplicaciones";
  }

  const isSubMenuSelected = (subMenu) => {
    setSelectedKeysMenu([`sub-${subMenu.nombreModulo}`]);
  }

  return (
    <Sider
      collapsible style={{ background: "#fff" }} width={230}
    >
      {/* <span className={'ant-layout-sider-zero-width-trigger ant-layout-sider-zero-width-trigger-left'}>
        <Icon type={'bars'} style={{ backgroundColor: 'red' }} onClick={() => setCollapsed(!collapsed)} />
      </span> */}


      <Menu selectedKeys={selectedKeysMenu} mode="inline">
        {/* <Link to="/sesiones/aplicaciones"> */}
        <Button
          type="secondary"
          className="ant-btn sider-btn"
          key={1}
          style={{
            marginLeft: "15px",
            background: "transparent",
            border: "none",
            boxShadow: "none"
          }}
          onClick={onClick}
        >
          <HomeOutlined style={{
            position: "relative",
            left: "0",
            top: "-3px",
            color: "#D5007F",
            fontSize: "16px",
            right: "5px",
            marginRight: "10px",
            marginBottom: "10px"
          }}
            size={35} />
          <a
            style={{ color: '#D5007F', textDecoration: 'underline', fontSize: '16px' }}>Volver al inicio</a>
        </Button>

        {/* </Link> */}
        {menu.map((itemMenu) => (
          <SubMenu key={itemMenu.idMenu} title={itemMenu.nombreMenu}>
            {itemMenu.subMenus[0].modulos.map((subMenu) => (
              <Menu.Item disabled={props.distritoFedSelec != null ? false : true} key={`sub-${subMenu.nombreModulo}`} onClick={() => isSubMenuSelected(subMenu)}>
                <Link to={subMenu.urlModulo}>{subMenu.nombreModulo}</Link>
              </Menu.Item>
            ))}
          </SubMenu>
        ))}
      </Menu>
      <Divider style={{ background: "none" }} />
      <Divider style={{ background: "none" }} />
      <Divider style={{ background: "none" }} />
      <Divider style={{ background: "none" }} />
      <div className="siderButtons">
        {/* <div style={{ paddingBottom: "10px" }} align="center">
          <Button
            type="secondary"
            className="login-form-button"
            tabIndex="3"
            style={{
              width: "77%",
              position: "relative",
              top: "80px",
              //left: "25px",
              transform: "translateY(+29.5%)",
              fontSize: "15px",
            }}
          >
            <BarChartOutlined
              className="login-form-button"
              style={{
                position: "relative",
                top: "8.5px",
                transform: "translateY(-69.5%)",
                fontSize: "15px",
              }}
            />
            Reportes
          </Button>
        </div> */}
        <div style={{ paddingBottom: "10px" }} align="center">
          <Button
            type="primary"
            className="login-form-button hideOnDesktop "
            tabIndex="4"
            style={{
              width: "77%",
              position: "relative",
              top: "100px",
              //left: "25px",
              transform: "translateY(+29.5%)",
              fontSize: "15px",
            }}
            onClick={handleClickOpen}
          >
            <CloseCircleOutlined
              style={{
                position: "relative",
                top: "8.5px",
                transform: "translateY(-69.5%)",
                fontSize: "15px",
              }}
            />
            Cerrar sesión
          </Button>
          <Dialog open={alertOpen} onClose={handleClose}>
            <DialogContent>Deseas Cerrar Sesión</DialogContent>
            <DialogActions>
              <Button onClick={logout}>Aceptar</Button>
              <Button onClick={handleClose}>Cancelar</Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
    </Sider >
  );
}
