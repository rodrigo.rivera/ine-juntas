import {codificarBase64, decodificarBase64} from '../Helpers/Helpers'
/**
 * Proposito: Cargar el estado desde el session storage.
 * @author karen.vazquez, INE
 */
export const cargarEstado = () => {

    try{

        const estadoSerializado = localStorage.getItem('state');

        if (estadoSerializado === null){

            return undefined;
        
        } else {


            return JSON.parse(decodificarBase64(estadoSerializado));
        }

    }catch (err){

        return undefined;
    }
};

/**
 * Proposito: Guardar el estado de la aplicacion en el session storage.
 * @author karen.vazquez, INE
 * @param {*} estado Estado de la aplicacion.
 */
export const guardarEstado = (estado) => {

    try{

        if(estado.dataLogin.isAuthenticated === false){

            localStorage.clear();
        
        } else {

            const estadoSerializado = codificarBase64(JSON.stringify(estado));

            localStorage.setItem('state' , estadoSerializado);
        }
       

    } catch ( err){

    }
};

/**
 * Proposito: Remover el estado el estado de la aplicacion en el session storage.
 * @author karen.vazquez, INE
 * @param {*} estado Estado de la aplicacion.
 */
export const removerEstado = (estado) => {

    try{

       
        localStorage.removeItem(estado);
       

    } catch ( err){

    }
};
