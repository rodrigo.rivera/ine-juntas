import React, { useState, useEffect } from 'react'
import { Tabs } from 'antd';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { FileAddOutlined, FormOutlined } from '@ant-design/icons';
import Fomulario from "./VocalesFormulario";
import FomularioEditar from "./VocalesFormularioEditar";
import Home from "./VocalesHome";
import Mostrar from "./VocalesMostrar";
import Baja from "./VocalesBaja";
import axios from "axios";
import { useHistory } from "react-router-dom";
//import Imagen from../../Components/CargarImagen/imageneses";
const { TabPane } = Tabs;


export default function VocalesInit(props) {
    const history = useHistory();
    //console.log(props);
    //const { menu } = props.menu;
    const [menu, ht] = useState(props.menu);
    const [tempProfesiones, setTempProfesiones] = useState(null);
    const [tempTratamiento, setTempTratamiento] = useState(null);
    const [listaPuestos, setListaPuestos] = useState(null);

    useEffect(() => {
        //console.log(dataMenu.infoMenu);        


        if (menu.infoMenu.distritoFedSelec != null) {
            //console.log("paso  condicon")

            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
                .then((response) => {
                    // console.log("urls::::::::::");
                    //console.log(response);
                    let token = localStorage.getItem('accessToken');
                    localStorage.setItem('aplicaciones', response.data.urlLogin);
                    if (response.status === 200) {
                        //setUrls(response.data);
                        //console.log(response)
                        localStorage.setItem("idDistrito", menu.infoMenu.distritoFedSelec.idDistrito);
                        localStorage.setItem("idEstado", menu.infoMenu.estadoSelec.idEstado);
                        axios.post(`${response.data.centralSesiones}/vocales/consulta`, {
                            //"idDetalleProceso": 106,
                            "idDistrito": menu.infoMenu.distritoFedSelec.idDistrito,
                            //"idDistrito":5,
                            "idEstado": menu.infoMenu.estadoSelec.idEstado,
                        }, {
                            headers: {
                                Authorization: token,
                            },
                        }).then((respuesta) => {
                            //localStorage.setItem('accessToken', respuesta.data.newToken);
                            console.log("----------------",respuesta)
                            //console.log(respuesta.data.vocalesHome)

                            setTempTratamiento(respuesta.data.tratamiento);
                            setTempProfesiones(respuesta.data.profesiones)
                            setListaPuestos(respuesta.data.puestos);

                            localStorage.setItem("tramiento", JSON.stringify(respuesta.data.tratamiento))
                            localStorage.setItem("profesiones", JSON.stringify(respuesta.data.profesiones))
                            localStorage.setItem("puestos", JSON.stringify(respuesta.data.puestos))
                            localStorage.setItem("motivos", JSON.stringify(respuesta.data.motivosBaja))
                        }).catch((erorr) => {
                            console.log("eroror ", erorr)
                        });
                    }
                });
        }

    }, []);
    return (
        <>
            {
                localStorage.getItem("idDistrito") != null ?
                    <div className="containerLista " >

                        <br />
                        <div>
                            <Router>
                                <switch>
                                    <Route path="/vocales/crear" >
                                        <Fomulario menu={menu} estatus={true} completo={null} history={history} />
                                    </Route>
                                    <Route path="/vocales/home" >
                                        <Home menu={menu} />
                                    </Route>
                                    <Route path="/vocales/:id/editar" >
                                        <FomularioEditar menu={menu} history={history} />
                                    </Route>
                                    <Route path="/vocales/:id/mostrar"  >
                                        <Mostrar menu={menu} tempProfesiones={tempProfesiones} tempTratamiento={tempTratamiento} listaPuestos={listaPuestos} />
                                    </Route>
                                    <Route path="/vocales/:id/remplazar" >
                                        <Baja menu={menu} />
                                    </Route>
                                </switch>
                            </Router>

                        </div>
                    </div >

                    : null
            }
            {/* */}
        </>

    )
}