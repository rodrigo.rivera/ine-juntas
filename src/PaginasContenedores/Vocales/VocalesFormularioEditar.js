import React, { useState, useEffect } from "react";

import { Table, Tooltip, Steps, Button, Select, Radio, DatePicker, Checkbox, Spin, Icon } from 'antd';
import InputINE from '../../Components/Input/inputINE';
import CargarImagen from "../../Components/Imagenes/CargarImagen"
import CargarArchivo from "../../Components/Imagenes/cargarArchivos"
import { getCodigoPolstal } from "../../Components/CodigoPostal/CodigoPostal";
import { TablaRegistro } from "../../Components/Tablas/VocalFormulario";
import { browserHistory } from "react-router";
import Telefonos from "../../Components/Telefonos/telefonosEditar";
import { useParams, Link } from 'react-router-dom';
import Experiencia from "../../Components/Experiencia/VocalesExperieciaEdit";
import '../../Assets/css/vocales.css';
import moment from "moment";
import axios from 'axios';
import { Dialog, DialogContent, DialogActions } from "@material-ui/core";
import Estados from "../../Assets/files/Estados.json";
import alertINE from "../../Components/Alert/AlertsINE";
//import { UploadOutlined } from '@ant-design/icons';


const { Column } = Table;
const { Step } = Steps;
const { Option } = Select;
const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;


export default function VocalesFormulario(props) {
    let { id } = useParams();
    const { history } = props

    const [loadingEenviar, setLoadingEnviar] = useState(false);
    const [dataMenu] = useState(props.menu)
    const [tempProfesiones, setTempProfesiones] = useState(JSON.parse(localStorage.getItem("profesiones")));
    const [tempTratamiento, setTempTratamiento] = useState(JSON.parse(localStorage.getItem("tramiento")));
    const [listaPuestos, setListaPuestos] = useState(JSON.parse(localStorage.getItem("puestos")));
    const [tempCargo, setCargo] = useState([
        {
            id: 1,
            descripcion: "VOCAL EJECUTIVA/O"
        },
        {
            id: 2,
            descripcion: "VOCAL SECRETARIA/O"
        },
        {
            id: 3,
            descripcion: "VOCAL DE ORGANIZACIÓN ELECTORAL"
        },
        {
            id: 4,
            descripcion: "VOCAL DEL REGISTRO FEDERAL DE ELECTORES"
        },
        {
            id: 5,
            descripcion: "VOCAL DE CAPACITACIÓN ELECTORAL Y EDUCACIÓN CÍVICA"
        },
        {
            id: 6,
            descripcion: "ENCARGADA/O DEL DESPACHO DEL VE"
        }]);

    const [editar] = useState(false);
    const [edit, setEdit] = useState(null);

    const [urlImgen, setUrlImg] = useState(null);
    const [urlArchvivo, setUrlINombramiento] = useState(null);
    const [urlFirma, setUrlIFirma] = useState(null);

    useEffect(() => {
        console.log(dataMenu.infoMenu);
        if (dataMenu.infoMenu.distritoFedSelec != null && dataMenu.infoMenu.estadoSelec) {
            //console.log("paso!")

            axios.get(window.location.origin + '/JsonHelpers/jsonUrl.json').then((response) => {
                // console.log("urls::::::::::");
                console.log(response);
                localStorage.setItem('aplicaciones', response.data.urlLogin);
                let token = localStorage.getItem('accessToken');
                console.log('el token para insertar DomiciliosDeJunta: ', token);
                if (response.status === 200) {
                    //setUrls(response.data);
                    let data = {
                        idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                        idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                        modulo: "Vocales",
                        idSubModulo: id,
                        nombreSistema: "junta",
                        modificar: true,
                    }
                    console.log("Mostrar datos ", data)
                    axios.post(`${response.data.centralSesiones}/vocales/descargaArchivo`, data, {
                        headers: {
                            Authorization: token,
                        },
                    })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);
                            console.log("la data ", respuesta)
                            let lista = base64ToBlobs(respuesta.data.archivo)
                            console.log(lista.blobs);
                            setUrlImg(lista.blobs[0].source)
                            // setUrlImagen(lista.blobs[0].source);
                            setUrlINombramiento(lista.blobs[1].source);
                            setUrlIFirma(lista.blobs[2].source);

                        })
                        .catch((erorr) => {
                            console.log('eroror ', erorr);
                        });



                    axios
                        .post(`${response.data.centralSesiones}/vocales/getVocal`, {
                            "idDistrito": dataMenu.infoMenu.distritoFedSelec.idDistrito,
                            "idVocal": id,
                            "idEstado": dataMenu.infoMenu.estadoSelec.idEstado,
                        }, {
                            headers: {
                                Authorization: token,
                            },
                        })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);
                            setEdit(respuesta.data.vocal);

                        })
                        .catch((erorr) => {
                            console.log('eroror ', erorr);
                        });
                }
            });
        }
        //setTest("valeria")

    }, []);

    const base64ToBlobs = (base64Images) => {
        console.log("recibe ", base64Images)
        var images = { blobs: [] }

        for (var i = 0; i < base64Images.length; i++) {
            //console.log(base64Images[i].tipoArchivo)
            if (base64Images[i].archivoBase64 !== null) {
                images.blobs.push({
                    "source": b64toBlob(base64Images[i].archivoBase64, base64Images[i].tipoArchivo),
                    "name": base64Images[i].nombreArchivo.split('-')[0],
                    "order": base64Images[i].nombreArchivo.split('-')[1]
                });
            }

        }
        console.log("Archivos:::::::::::::::::::::::")
        console.log(images)
        return images
    };

    function b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');
        var byteCharacters = window.atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = URL.createObjectURL(new Blob(byteArrays, { type: contentType }));
        return blob;
    }


    const getCargos = (data) => {

        let lista = [
            {
                id: 1,
                descripcion: "VOCAL EJECUTIVA/O"
            },
            {
                id: 2,
                descripcion: "VOCAL SECRETARIA/O"
            },
            {
                id: 3,
                descripcion: "VOCAL DE ORGANIZACIÓN ELECTORAL"
            },
            {
                id: 4,
                descripcion: "VOCAL DEL REGISTRO FEDERAL DE ELECTORES"
            },
            {
                id: 5,
                descripcion: "VOCAL DE CAPACITACIÓN ELECTORAL Y EDUCACIÓN CÍVICA"
            },
            {
                id: 6,
                descripcion: "ENCARGADA/O DEL DESPACHO DEL VE"
            }]

        let temp = [];
        data.forEach(i => {
            temp.push(i.idCargo);
        })
        let faltan = [];
        lista.forEach(i => {
            let t = temp.indexOf(i.id);

            if (t == -1) {
                faltan.push(i);
            }
        });
        console.log(faltan)
        return faltan;
    }


    const [numeroPagina, setnumeroPagina] = useState({ pagina: 0 });
    /**
     * pagina 01
     */

    const [cargoDP, setCargoDP] = useState({ estatus: false, value: null })
    const [fotografiaDP, setFotografiaDP] = useState({ estatus: false, value: null })
    const [apellidoMaternoDP, setApellidoMaternoDP] = useState({ estatus: false, value: null });
    const [apellidoPaternoDP, setApellidoPaternoDP] = useState({ estatus: false, value: null });
    const [nombreDP, setNombreDP] = useState({ estatus: false, value: null });
    const [claveElectorDP, setClaveElectorDP] = useState({ estatus: false, value: null });
    const [curpDP, setCurpDP] = useState({ estatus: false, value: null });
    const [generoDP, setGeneroDP] = useState({ estatus: false, value: null });
    const [fechaNacimientoDP, setFechaNacimientoDP] = useState({ estatus: false, value: null });
    const [ciudadanaDP, setCiudadanaDP] = useState({ estatus: false, value: null });
    const [entidadDP, setEntidadDP] = useState({ estatus: false, value: null });
    const [municipioDP, setMunicipioDP] = useState({ estatus: false, value: null });
    const [profesionDP, setProfesionDP] = useState({ estatus: false, value: null });
    const [tratamientoDP, setTratamientoDP] = useState({ estatus: false, value: null });
    const [puestoDP, setPuestoDP] = useState({ estatus: false, value: null });
    //auxiliares
    const [llenarDistrito, setLlenarDistrito] = useState(null);
    const [auxCiudadano, setAuxCiudadano] = useState(false);
    /**
     * pagina 02
     */
    const [calleC, setCalleC] = useState({ estatus: false, value: null });
    const [numeroExteriorC, setNumeroExteriorC] = useState({ estatus: false, value: null });
    const [numeroInteriorC, setNumeroInteriorC] = useState({ estatus: false, value: null });
    const [sinNumeroC, setSinNumeroC] = useState({ estatus: false, value: false });
    const [codigoPostalC, setCodigoPostalC] = useState({ estatus: false, value: null });
    const [entidadFederativaC, setEntidadFederativaC] = useState({ estatus: false, value: null });
    const [municipioC, setMunicipioC] = useState({ estatus: false, value: null });

    //auxiliares
    const [apiCodigo, setApiCodigo] = useState(false);
    const [colonias, setColonias] = useState(null)
    const [coloniaC, setColoniaC] = useState({ estatus: false, value: null });
    const [correoC, setCorreoC] = useState({ estatus: false, value: null });
    const [telefonoC, setTelefonoC] = useState({ estatus: false, value: null });
    /**
     * pagina 03
     */
    const [fechaIngresoEs, setFechaIngresoEs] = useState({ estatus: false, value: null });
    const [fechaNombramientoEs, setFechaNombramientoEs] = useState({ estatus: false, value: null });
    const [fechaTitularidadEs, setFechaTitularidadEs] = useState({ estatus: false, value: null });
    const [nombramientoEs, setNombramientoEs] = useState({ estatus: false, value: null });
    const [puestoEs, setPuestoEs] = useState({ estatus: false, value: null })
    const [firmaEs, setFirmaEs] = useState({ estatus: false, value: null })
    /**
     * pagina 04
     */
    const [experiencia, setExperiencia] = useState({ estatus: false, value: null })
    const [experienciaSend, setExperienciaSend] = useState({ estatus: false, value: null })


    const [guardar, setGuardar] = useState(false);



    /**
     * pagina 02
     */

    const [checkBoxNum, setCheckBoxNum] = useState(null)

    const changeTelefono = (data) => {
        console.log("telefonos fomrulario", data)
        setTelefonoC({ estatus: true, value: data })

    }

    /**
     * 
     */
    const changeExperiencia = (data) => {
        console.log("experiencia fomrulario", data)
        setExperiencia({ estatus: true, value: data })

    }

    const changeExperienciaSend = (data) => {
        console.log("EXperiencias send +", data)
        setExperienciaSend({ estatus: true, value: data })

    }
    /**
     * metodos botones
     */

    const next = () => {
        setnumeroPagina({ pagina: numeroPagina.pagina + 1 });

    }

    const prev = () => {
        setnumeroPagina({ pagina: numeroPagina.pagina - 1 });
    }

    const getEntidad = (id) => {
        // console.log("entidad   ",id)
        // console.log(Estados.estados[id]);
        //console.log("El", id)
        if (id != null) {
            return Estados.estados[id - 1].nombreEstado;

        } else {
            return "";
        }
    }

    const getDistrito = (idEstado, idDistrito) => {
        //console.log(Estados.estados[idEstado].distritos[idDistrito].nombreDistrito);

        if (idEstado != null && idDistrito != null) {
            //console.log("idEstado: ", idEstado, " distri: ", idDistrito, "==", Estados.estados[idEstado-1].distritos[idDistrito-1])

            if (Estados.estados[idEstado - 1].distritos[idDistrito - 1] == undefined) {
                return "distrito invalido"
            } else {
                return Estados.estados[idEstado - 1].distritos[idDistrito - 1].nombreDistrito;

            }
        } else {
            return ""
        }
    }

    const getFecha = (data) => {

        if (data != null && data.length > 2) {
            let tempFechca = data.split(" ")[0].split("-");
            //let tempfecha= tempFechca.split("-");
            let fecha = tempFechca[2] + "/" + tempFechca[1] + "/" + tempFechca[0];
            return fecha;
        } else {
            return null;
        }
        //return moment(new Date(data), "DD-MM-YYYY")
    }


    const getPuesto = (id) => {
        //console.log("puesto ", id)

        if (id != null) {
            return JSON.parse(localStorage.getItem("puestos"))[id - 1].descripcion

        } else {
            return "N/A";
        }

    }
    const addkeyExperienciaMostrar = (data) => {
        let index = 0;
        let lista = [];

        data.forEach(e => {
            let form = {
                "distrito": getDistrito(e.entidad, e.distrito),
                "entidad": getEntidad(e.entidad),
                "fechaIncio": getFecha(e.fechaIncio),
                "fechaTermino": getFecha(e.fechaTermino),
                "instituto": e.instituto == null ? null : (e.instituto == 1 ? "INE" : "OPL"),
                "key": index,
                "puesto": getPuesto(e.puesto),
                "rama": e.rama == null ? null : (e.rama == 1 ? "Administrativo" : "Servicio Profesional"),
            }
            index++;
            lista.push(form);
        });

        return lista;
    }

    const addkeyExperienciaEnviar = (data) => {
        let index = 0;
        let lista = [];

        data.forEach(e => {
            let form = {
                "distrito": e.distrito,
                "entidad": e.entidad,
                "fechaIncio": getFecha(e.fechaIncio),
                "fechaTermino": getFecha(e.fechaTermino),
                "instituto": e.instituto,
                "key": index,
                "puesto": e.puesto,
                "rama": e.rama
            }
            index++;
            lista.push(form);
        });

        return lista;
    }

    const enviar = () => {
        //console.log("Expericnia ", experienciaSend)
        //console.log((nombreDP.estatus ? true : (nombreDP.value != null ? false : true)))
        console.log(
            "nombreDP: ", (nombreDP.estatus ? true : (nombreDP.value != null ? false : true)), "\n",
            "curpDP: ", (curpDP.estatus ? true : (curpDP.value != null ? false : true)), "\n",
            "generoDP: ", (generoDP.estatus ? true : (generoDP.value != null ? false : true)), "\n",
            "fechaNacimientoDP: ", (fechaNacimientoDP.estatus ? true : (fechaNacimientoDP.value != null ? false : true)), "\n",
            "ciudadanaDP: ", (ciudadanaDP.estatus ? true : (ciudadanaDP.value != null ? false : true)), "\n",
            "entidadDP: ", (entidadDP.estatus ? true : (entidadDP.value != null ? false : true)), "\n",
            "municipioDP: ", (municipioDP.estatus ? true : (municipioDP.value != null ? false : true)), "\n",
            "v_profesion", (profesionDP.estatus ? true : (profesionDP.value != null ? false : true)), "\n",
            "v_tratamiento", (tratamientoDP.estatus ? true : (tratamientoDP.value != null ? false : true)), "\n",
            //pagina 2
            "calleC: ", (calleC.estatus ? true : (calleC.value != null ? false : true)), "\n",
            "codigoPostalC: ", (codigoPostalC.estatus ? true : (codigoPostalC.value != null ? false : true)), "\n",
            "entidadFederativaC: ", (entidadFederativaC.estatus ? true : (entidadFederativaC.value != null ? false : true)), "\n",
            "v_municipio", (municipioC.estatus ? true : (municipioC.value != null ? false : true)), "\n",
            "municipioC: ", (municipioC.estatus ? true : (municipioC.value != null ? false : true)), "\n",
            "coloniaC: ", (coloniaC.estatus ? true : (coloniaC.value != null ? false : true)), "\n",
            "correoC: ", (correoC.estatus ? true : (correoC.value != null ? false : true)), "\n",
            "fechaIngresoEs: ", (fechaIngresoEs.estatus ? true : (fechaIngresoEs.value != null ? false : true)), "\n",
            "puestoEs: ", (puestoEs.estatus ? true : (puestoEs.value != null ? false : true)), "\n",

            "telefonos", (telefonoC.estatus ? true : (telefonoC.value != null ? false : true)), "\n",
            "experiencia", (experienciaSend.estatus && experienciaSend.value.length > 0), "\n",

            "apellido:", ((apellidoMaternoDP.estatus ? true : (apellidoMaternoDP.value != null ? false : true)) ||
                (apellidoPaternoDP.estatus ? true : (apellidoPaternoDP.value != null ? false : true))), "\n",

        )

        let data = {
            /**
             * pagina 01
             */
            //puestoDP.value  // falta este dato
            "v_id": parseInt(id),
            "usuario": "rodrigo",
            "v_nombre": (nombreDP.estatus ? nombreDP.value : (nombreDP.value != null ? null : edit.v_nombre)),
            "v_apellidoPaterno": (apellidoMaternoDP.estatus ? apellidoMaternoDP.value : (apellidoMaternoDP.value != null ? null : edit.v_apellidoPaterno)),
            "v_apellidoMaterno": (apellidoPaternoDP.estatus ? apellidoPaternoDP.value : (apellidoPaternoDP.value != null ? null : edit.v_apellidoMaterno)),
            "v_claveElector": edit.v_claveElector,
            "v_curp": (curpDP.estatus ? curpDP.value : (curpDP.value != null ? null : edit.v_curp)),
            "v_genero": (generoDP.estatus ? generoDP.value : (generoDP.value != null ? null : edit.v_genero)),
            "v_fechaNacimiento": (fechaNacimientoDP.estatus ? fechaNacimientoDP.value : (fechaNacimientoDP.value != null ? null : getFecha(edit.v_fechaNacimiento))),
            "v_ciudadania": (ciudadanaDP.estatus ? ciudadanaDP.value : (ciudadanaDP.value != null ? null : edit.v_ciudadania)),
            "v_entidad": (entidadDP.estatus ? entidadDP.value : (entidadDP.value != null ? null : edit.v_entidad)),
            "v_municipio": (municipioDP.estatus ? municipioDP.value : (municipioDP.value != null ? null : edit.v_municipio)),
            "v_profesion": (profesionDP.estatus ? profesionDP.value : (profesionDP.value != null ? null : edit.v_profesion)),
            "v_tratamiento": (tratamientoDP.estatus ? tratamientoDP.value : (tratamientoDP.value != null ? null : edit.v_tratamiento)),
            "v_img": "fotografiaDP.value",
            "v_correoContacto": (correoC.estatus ? correoC.value : (correoC.value != null ? null : edit.v_correoContacto)),
            "p_puestoEjerce": (puestoDP.estatus ? puestoDP.value : (puestoDP.value != null ? null : edit.p_puestoEjerce)),
            /**
             * pagina 02
             */

            "d_calleContacto": (calleC.estatus ? calleC.value : (calleC.value != null ? null : edit.d_calleContacto)),
            "d_numeroExterior": (numeroExteriorC.estatus ? numeroExteriorC.value : (numeroExteriorC.value != null ? null : edit.d_numeroExterior)),
            "d_numeroInterior": (numeroInteriorC.estatus ? numeroInteriorC.value : (numeroInteriorC.value != null ? null : edit.d_numeroInterior)),
            "d_coloniaContacto": (coloniaC.estatus ? coloniaC.value : (coloniaC.value != null ? null : edit.d_coloniaContacto)),
            "d_idMunicipioContacto": -1,
            "d_municipioContacto": (municipioC.estatus ? municipioC.value : (municipioC.value != null ? null : edit.d_municipioContacto)),
            "d_codigoPostalContacto": (codigoPostalC.estatus ? codigoPostalC.value : (codigoPostalC.value != null ? null : edit.d_codigoPostalContacto)),
            "d_api": apiCodigo ? 1 : 0,
            "d_entidadContacto": (entidadFederativaC.estatus ? entidadFederativaC.value : (entidadFederativaC.value != null ? null : edit.d_entidadContacto)),
            "telefonos": telefonoC.value,

            /**
             * pagina 03
             */


            "p_id_Estado": dataMenu.infoMenu.estadoSelec.idEstado,
            "p_id_Distrito": dataMenu.infoMenu.distritoFedSelec.idDistrito,
            "p_puesto": (cargoDP.estatus ? cargoDP.value : (cargoDP.value != null ? null : edit.p_puesto)),
            "p_fechaIngreso": (fechaIngresoEs.estatus ? fechaIngresoEs.value : (fechaIngresoEs.value != null ? null : getFecha(edit.p_fechaIngreso))),
            "p_fechaNombramiento": (fechaNombramientoEs.estatus ? fechaNombramientoEs.value : (fechaNombramientoEs.value != null ? null : getFecha(edit.p_fechaNombramiento))),
            "p_fechaTitulariad": (fechaTitularidadEs.estatus ? fechaTitularidadEs.value : (fechaTitularidadEs.value != null ? null : getFecha(edit.p_fechaTitulariad))),
            "p_estatus": 1,
            "p_tipoAlta": null,
            "p_tipoPuesto": (puestoEs.estatus ? puestoEs.value : (puestoEs.value != null ? null : edit.p_tipoPuesto)),
            "p_nombramiento": (nombramientoEs.estatus ? "nombramientoEs.value" : (nombramientoEs.value != null ? null : edit.p_nombramiento)),

            "p_firma": "firmaEs.value",


            /**
             * pagina 04
             */
            "experiencia": experienciaSend.value
        }
        console.log("el json ", data);
        console.log("paso!" + fechaNacimientoDP.value != null)

        if (
            (nombreDP.estatus ? true : (nombreDP.value != null ? false : true)) &&
            (curpDP.estatus ? true : (curpDP.value != null ? false : true)) &&
            (generoDP.estatus ? true : (generoDP.value != null ? false : true)) &&
            (fechaNacimientoDP.estatus ? true : (fechaNacimientoDP.value != null ? false : true)) &&
            (ciudadanaDP.estatus ? true : (ciudadanaDP.value != null ? false : true)) &&
            (entidadDP.estatus ? true : (entidadDP.value != null ? false : true)) &&
            (municipioDP.estatus ? true : (municipioDP.value != null ? false : true)) &&
            (profesionDP.estatus ? true : (profesionDP.value != null ? false : true)) &&
            (tratamientoDP.estatus ? true : (tratamientoDP.value != null ? false : true)) &&
            (calleC.estatus ? true : (calleC.value != null ? false : true)) &&
            (codigoPostalC.estatus ? true : (codigoPostalC.value != null ? false : true)) &&
            (entidadFederativaC.estatus ? true : (entidadFederativaC.value != null ? false : true)) &&
            (municipioC.estatus ? true : (municipioC.value != null ? false : true)) &&
            (municipioC.estatus ? true : (municipioC.value != null ? false : true)) &&
            (coloniaC.estatus ? true : (coloniaC.value != null ? false : true)) &&
            (correoC.estatus ? true : (correoC.value != null ? false : true)) &&
            (fechaIngresoEs.estatus ? true : (fechaIngresoEs.value != null ? false : true)) &&
            (puestoEs.estatus ? true : (puestoEs.value != null ? false : true)) &&
            (telefonoC.estatus && telefonoC.value.length > 0) &&
            (experienciaSend.estatus && experienciaSend.value.length > 0) &&
            ((apellidoMaternoDP.estatus ? true : (apellidoMaternoDP.value != null ? false : true)) ||
                (apellidoPaternoDP.estatus ? true : (apellidoPaternoDP.value != null ? false : true)))
        ) {


            console.log("la Data que se va enviar  ", data);
            setLoadingEnviar(true)
            axios.get(window.location.origin + '/JsonHelpers/jsonUrl.json').then((response) => {
                // console.log("urls::::::::::");
                //console.log(response);
                localStorage.setItem('aplicaciones', response.data.urlLogin);
                let token = localStorage.getItem('accessToken');
                //console.log('el token para insertar DomiciliosDeJunta: ', token);
                if (response.status === 200) {
                    //setUrls(response.data);
                    axios
                        .post(`${response.data.centralSesiones}/vocales/actualizar`, data, {
                            headers: {
                                Authorization: token,
                            },
                        })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);
                            console.log("--------------Respuesta", respuesta.data)
                            if (respuesta.data.code == 200) {
                                //alertINE("success", "Se agrego correctamente.");
                                generarFormImagenes();

                            } else {
                                alertINE("error", "Ocurrio un error al insertar.");
                            }

                        })
                        .catch((error) => {
                            console.log("error ", error)
                            alertINE("error", "Ocurrio un error en la conexion,\nno se pudo insertar");
                        });
                }
            });

        } else {
            alertINE("error", "Debes llenar todos los campos.");
        }

    }


    /**
     * genera el obejeto form dependiendo que fotos cambio 
     * metodo que se me ocurrio, suponiendo que todo se cambio hasta que solo se cambio uno
     */
    const generarFormImagenes = () => {

        const formData = new FormData();



        console.log("fotografiaD: ", fotografiaDP.estatus, "\n   :firmaEs:", firmaEs.estatus, "\n   :nombramientoEs:", nombramientoEs.estatus)

        /**
                 * caso cambio foto firma nombramiento 
                 */
        if (fotografiaDP.estatus && firmaEs.estatus && nombramientoEs.estatus) {
            const listaNombres = ["0.png", "1.pdf", "2.png"];
            formData.append('files', fotografiaDP.value.originFileObj);
            formData.append('files', nombramientoEs.value.originFileObj);
            formData.append('files', firmaEs.value.originFileObj);

            formData.append('param', new Blob([JSON.stringify({
                idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                modulo: "Vocales",
                idSubModulo: parseInt(id),
                nombreSistema: "junta",
                modificar: true,
                nombreArchivo: listaNombres,
            })], { type: "application/json" }));

            console.log("el formData ", formData);
            guardaArchivo(formData).then((data) => {
                //alertINE("success", "Seguardo con exito")
                console.log("se guardo con exito", data);
                setGuardar(true);
            }).catch((error) => {
                console.log("error guardar !", error)
            });
            return formData;

        }

        if (fotografiaDP.estatus || (firmaEs.estatus && nombramientoEs.estatus)) {
            // cambia foto y firma 
            if (firmaEs.estatus && fotografiaDP.estatus) { // caso cambio 2 
                console.log("cambio foto firma ")

                const listaNombres = ["0.png", "2.png"];
                formData.append('files', fotografiaDP.value.originFileObj);
                formData.append('files', firmaEs.value.originFileObj);

                formData.append('param', new Blob([JSON.stringify({
                    idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                    idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                    modulo: "Vocales",
                    idSubModulo: parseInt(id),
                    nombreSistema: "junta",
                    modificar: true,
                    nombreArchivo: listaNombres,
                })], { type: "application/json" }));

                console.log("el formData ", formData);
                guardaArchivo(formData).then((data) => {
                    //alertINE("success", "Seguardo con exito")
                    setGuardar(true)
                }).catch((error) => {
                    console.log("error guardar !", error)
                });
                return formData;
            }
            // cambia foto y  documetno 
            if (nombramientoEs.estatus && fotografiaDP.estatus) { // camibo dos 
                console.log("cambio foto nombramiento ");
                const listaNombres = ["0.png", "1.pdf"];
                formData.append('files', fotografiaDP.value.originFileObj);
                formData.append('files', nombramientoEs.value.originFileObj);

                formData.append('param', new Blob([JSON.stringify({
                    idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                    idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                    modulo: "Vocales",
                    idSubModulo: parseInt(id),
                    nombreSistema: "junta",
                    modificar: true,
                    nombreArchivo: listaNombres,
                })], { type: "application/json" }));

                console.log("el formData ", formData);
                guardaArchivo(formData).then((data) => {
                    //alertINE("success", "Seguardo con exito")
                    setGuardar(true)
                }).catch((error) => {
                    console.log("error guardar !", error)
                });
                return formData;

            }
            if (fotografiaDP.value) {//caso cambio solo una 
                console.log("cambio foto ")
                const listaNombres = ["0.png"];
                formData.append('files', fotografiaDP.value.originFileObj);
                formData.append('param', new Blob([JSON.stringify({
                    idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                    idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                    modulo: "Vocales",
                    idSubModulo: parseInt(id),
                    nombreSistema: "junta",
                    modificar: true,
                    nombreArchivo: listaNombres,
                })], { type: "application/json" }));

                console.log("el formData ", formData);
                guardaArchivo(formData).then((data) => {
                    // alertINE("success", "Seguardo con exito")
                    setGuardar(true)
                }).catch((error) => {
                    console.log("error guardar !", error)
                });
                return formData;
            }

        }

        if (nombramientoEs.estatus || (firmaEs.estatus && fotografiaDP.estatus)) {
            // cambia nombramiento y firma 
            if (nombramientoEs.estatus && firmaEs.estatus) { // caso cambio 2 
                console.log("cambio nombramiento y   firma ")
                const listaNombres = ["1.pdf", "2.png"];
                formData.append('files', nombramientoEs.value.originFileObj);
                formData.append('files', firmaEs.value.originFileObj);

                formData.append('param', new Blob([JSON.stringify({
                    idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                    idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                    modulo: "Vocales",
                    idSubModulo: parseInt(id),
                    nombreSistema: "junta",
                    modificar: true,
                    nombreArchivo: listaNombres,
                })], { type: "application/json" }));

                console.log("el formData ", formData);
                guardaArchivo(formData).then((data) => {
                    //alertINE("success", "Seguardo con exito")
                    setGuardar(true);
                }).catch((error) => {
                    console.log("error guardar !", error)
                });
                return formData;
            }

            // cambia nombramiento 
            if (nombramientoEs.value) {//caso cambio solo una 
                console.log("cambio nombremiento")
                const listaNombres = ["1.pdf"];
                formData.append('files', nombramientoEs.value.originFileObj);

                formData.append('param', new Blob([JSON.stringify({
                    idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                    idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                    modulo: "Vocales",
                    idSubModulo: parseInt(id),
                    nombreSistema: "junta",
                    modificar: true,
                    nombreArchivo: listaNombres,
                })], { type: "application/json" }));

                console.log("el formData ", formData);
                guardaArchivo(formData).then((data) => {
                    alertINE("success", "Seguardo con exito")
                    setGuardar(true)
                }).catch((error) => {
                    console.log("error guardar !", error)
                });
                return formData;
            }

        }


        if (firmaEs.estatus) {
            console.log("solo cambio firma ");
            const listaNombres = ["2.png"];

            formData.append('files', firmaEs.value.originFileObj);

            formData.append('param', new Blob([JSON.stringify({
                idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                modulo: "Vocales",
                idSubModulo: parseInt(id),
                nombreSistema: "junta",
                modificar: true,
                nombreArchivo: listaNombres,
            })], { type: "application/json" }));

            console.log("el formData ", formData);
            guardaArchivo(formData).then((data) => {
                //alertINE("success", "Seguardo con exito")
                setGuardar(true)
            }).catch((error) => {
                console.log("error guardar !", error)
            });
            return formData;

        }

        setLoadingEnviar(false);
        setGuardar(true);

    }

    const guardaArchivo = (ImagenesArchivos) => {
        return new Promise((resolve, reject) => {
            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json").then((response) => {
                // console.log(response);
                if (response.status === 200) {
                    const url = response.data.serverGeneral + "/guardarArchivo";
                    let token = localStorage.getItem("accessToken");
                    console.log("el token para insertar DomiciliosDeJunta: ", token);

                    if (token !== undefined && token != null) {
                        console.log("paso a el token ", ImagenesArchivos);
                        axios
                            .post(url, ImagenesArchivos, {
                                headers: {
                                    Authorization: token,
                                },
                            })
                            .then((response) => {
                                // console.log("Exito sevice ", response);
                                localStorage.setItem("accessToken", response.data.token);
                                const temp = response.data;
                                setLoadingEnviar(false);
                                resolve({ err: false, data: temp });
                            })
                            .catch((error) => {
                                setLoadingEnviar(false)
                                alertINE("error", "Ocurrio un errror ")
                                console.log("Errror ! service  guardarARchivo", error)

                                resolve({
                                    err: true,
                                    mensaje: error,
                                });
                            });
                    }
                }
            });
        });
    }

    /**
     * valida si lo que ingresa es valido 
     * @param {*} content 
     * @param {*} id 
     * @param {*} name 
     * @param {*} regex 
     * @param {*} warningMessage 
     * @param {*} disabled 
     */
    const handleInput = (content, id, name, regex, warningMessage, disabled) => {
        //console.log("disabled pppp " + disabled)
        //setCheckBoxNum(disabled)
        //console.log(regex)
        var regexResult = regex.test(content);
        switch (name) {
            case "apellidoPaterno":
                //console.log("entro apellido",regexResult, "contenid ",content)
                setApellidoPaternoDP({ estatus: regexResult, value: content });
                break;
            case "apellidoMaterno":
                setApellidoMaternoDP({ estatus: regexResult, value: content });
                break;
            case "nombre":
                setNombreDP({ estatus: regexResult, value: content });
                break;
            case "claveElector":
                setClaveElectorDP({ estatus: regexResult, value: content });
                break;
            case "curp":
                setCurpDP({ estatus: regexResult, value: content })
                break;
            case "puestoEjerce":
                setPuestoDP({ estatus: regexResult, value: content })
                break;
            case "calle":
                setCalleC({ estatus: regexResult, value: content })
                break;
            case "numeroExt":
                setNumeroExteriorC({ estatus: regexResult, value: content })
                break;
            case "numeroInt":
                setNumeroInteriorC({ estatus: regexResult, value: content })
                break;
            case "codigoPostal":

                if (regexResult) {
                    //console.log('**********************' + content + ' exito'
                    setCodigoPostalC({ estatus: true, value: content })


                    getCodigoPolstal(content).then((data) => {
                        if (!data.error) {
                            console.log("se encontro ene el api");
                            setEntidadFederativaC({ estatus: true, value: data.entidad });

                            setMunicipioC({ estatus: true, value: data.municipio });

                            setColonias(data.colonias);
                            setApiCodigo(true);
                        } else {
                            console.log("no se encontro en el api ");
                            setEntidadFederativaC({ estatus: false, value: null });
                            setMunicipioC({ estatus: false, value: null });
                            setColoniaC({ estatus: false, value: null });
                            setApiCodigo(false);
                        }
                    });
                }
                break;
            case "entidad":
                setEntidadFederativaC({ estatus: regexResult, value: content })
                break;
            case "municipio":
                setMunicipioC({ estatus: regexResult, value: content })

                break;
            case "colonia":
                setColoniaC({ estatus: regexResult, value: content })

                break;

            case "correo":
                setCorreoC({ estatus: regexResult, value: content })

                break;


            default:
                break;
        }

    }

    /**
     * 
     * eventos que cambian 
     *  
     */



    const changeCargo = e => {
        console.log("cargo ", e)
        setCargoDP({ estatus: true, value: e })
    }

    const changeGenero = e => {
        //console.log(e)
        console.log("genero ", e.target.value)
        setGeneroDP({ estatus: true, value: e.target.value })
    }
    const changeCiudanania = e => {
        console.log("ciudano ", e.target.value)
        if (e.target.value === 3) {

            setEntidadDP({ estatus: false, value: null });
            setMunicipioDP({ estatus: false, value: null })
            setCiudadanaDP({ estatus: true, value: e.target.value })
            setAuxCiudadano(true)
        } else {

            setAuxCiudadano(false)
            setCiudadanaDP({ estatus: true, value: e.target.value })
        }
        //setCiudadanaDP({ estatus: true, value: e.target.value })
    }

    const changeFechadeNacimiento = (data, dateString) => {
        console.log("fecha Nacimeinto ", dateString)

        if (dateString.length > 0) {
            console.log("ppaso la ffecha")
            setFechaNacimientoDP({ estatus: true, value: dateString });
        }
    }
    const changeEntidad = e => {
        console.log("enrtidad ", e)
        setLlenarDistrito(e)
        setMunicipioDP({ estatus: false, value: null })
        setEntidadDP({ estatus: true, value: e })
    }

    const changeMunicipio = e => {
        console.log("Municipio ", e)
        setMunicipioDP({ estatus: true, value: e })
    }
    const changeProfesion = e => {
        console.log("profesion ", e)
        setProfesionDP({ estatus: true, value: e })
    }
    const changeTratamiento = e => {
        console.log("tratamiento ", e)
        setTratamientoDP({ estatus: true, value: e })
    }

    const onChangeCheckBoxNum = e => {
        console.log("checbox ", e)
        setCheckBoxNum(e.target.checked);
        if (checkBoxNum) {
            console.log("no esta  seleccionado")
            //   setNumExt('');
            //   setNumInt('');
            setNumeroExteriorC({ estatus: false, value: null });
            setNumeroInteriorC({ estatus: false, value: null });
            setSinNumeroC({ estatus: false, value: e.target.check })
        } else {
            console.log("esta seleccionado ")
            //   setNumExt('S/N');
            //   setNumInt('S/N');
            setSinNumeroC({ estatus: true, value: null })
            setNumeroExteriorC({ estatus: true, value: null });
            setNumeroInteriorC({ estatus: true, value: null });
        }
        //setSinNumeroC({ estatus: !e.target.check, value: e.target.check })

    }

    const onChangeCodigoPostal = e => {
        console.log("codigo postal, ", e)

    }
    const selectColonia = e => {
        console.log("selectColonia, ", e)
        setColoniaC({ estatus: true, value: e })
    }
    /**
     * 
     * @param {*} data 
     * @param {*} dateString texto con el formato 
     */


    /**
     * 
     * @param {*} data 
     * @param {*} dateString  el texto con el formato 
     */
    const changeFechaIngreso = (data, dateString) => {
        console.log("fehca Ingreso ", dateString)
        if (dateString.length > 0) {
            console.log("ppaso la ffecha")
            setFechaIngresoEs({ estatus: true, value: dateString });
        }

    }

    /**
     * 
     * @param {*} data 
     * @param {*} dateString el texto con el formato 
     */
    const changeFechaNombramiento = (data, dateString) => {
        console.log("fecha Nombramiento ", dateString)
        if (dateString.length > 0) {

            setFechaNombramientoEs({ estatus: true, value: dateString });
        } else {
            setFechaNombramientoEs({ estatus: true, value: null });

        }
    }

    /**
     * 
     * @param {*} data 
     * @param {*} dateString 
     */
    const changeFechaTitulariad = (data, dateString) => {
        console.log("fecha de titulariad ", dateString);
        if (dateString.length > 0) {

            setFechaTitularidadEs({ estatus: true, value: dateString });
        } else {
            setFechaTitularidadEs({ estatus: true, value: null });

        }

    }
    /**
     * 
     * @param {*} e 
     */
    const changePuesto = e => {
        console.log("puesto ", e.target.value)
        setPuestoEs({ estatus: true, value: e.target.value });
    }


    const changeImagen = e => {
        console.log(e)
        setFotografiaDP({ estatus: true, value: e })

    }


    const changeFirma = e => {
        //console.log("valor de la img ", data)
        setFirmaEs({ estatus: true, value: e })
    }

    const changeArchivo = e => {
        console.log("el arvhvio ", e);
        setNombramientoEs({ estatus: true, value: e })
    }

    const cargarImagen = (data) => {
        console.log("cargar", data);

    }
    return (
        <>
            {loadingEenviar ? <div className="loading-style">
                <Spin indicator={antIcon} tip="Guardando..."></Spin>
            </div> :
                <div style={{ "paddingTop": "60px" }} className="containerLista container" style={{ paddingLeft: "50px", paddingRight: "50px" }}>
                    {
                        editar ? null : <Link to={{
                            pathname: `/vocales/home`
                        }
                        } >

                            <u className="letras" ><i className="iconAtras" />Regresar </u>
                        </Link>
                    }
                    <h1 style={{
                        "font-size": "24px",
                        'paddingLeft': '30px',
                        "paddingBottom": "50px"
                    }}>Editar Vocal</h1>




                    <div>
                        <div className="pasos">
                            <Steps className="pasos" current={numeroPagina.pagina} >

                                <Step title="Datos Personales" />
                                <Step title="Contacto" />
                                <Step title="Estatus" />
                                <Step title="Experiencia" />

                            </Steps>
                        </div>
                        {/*
            pagina datos Personales
          */}
                        {numeroPagina.pagina === 0 &&
                            <div className="col-12" style={{ paddingTop: "30px" }}>

                                <div className="center-block col-md-12 col-sm-12"  style={{ paddingLeft: "45%" }}>
                                    <CargarImagen getData={changeImagen} ponerImagen={fotografiaDP.value} editar={urlImgen} texto={"Fotografia"} />
                                </div>
                                <div className="row">
                                    <div className="col-md-4 col-sm-12">

                                        <font color="#d5007f">*</font>Cargo
                    <br />
                                        <Select onChange={changeCargo}
                                            style={{ width: '100%' }} disabled={true} value={cargoDP.estatus ? cargoDP.value : (edit != null ? edit.p_puesto : 'Selecciona una opción')}
                                            placeholder="Selecciona una opción">
                                            {
                                                tempCargo.map((tem) => (
                                                    <Select.Option value={tem.id}>{tem.descripcion}</Select.Option>
                                                ))

                                            }
                                        </Select>

                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">

                                        <Tooltip placement="right" title="Al menos uno de los apellidos es obligatorio">
                                            <i className="tooltip-apellido" />
                                        </Tooltip>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4 col-sm-12">
                                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label='Apellido paterno' name='apellidoPaterno'
                                            placeHolder='Apellido paterno'
                                            value={apellidoPaternoDP.estatus ? apellidoPaternoDP.value : (edit != null ? edit.v_apellidoPaterno : '')}
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Apellido paterno con caracteres inválidos'
                                            tooltipMessage='Apellido paterno'
                                            paddingLeft='3px' />
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label='Apellido materno' name='apellidoMaterno'
                                            placeHolder='Apellido materno'
                                            value={apellidoMaternoDP.estatus ? apellidoMaternoDP.value : (edit != null ? edit.v_apellidoMaterno : '')}
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Apellido materno con caracteres inválidos'
                                            tooltipMessage='Apellido materno'
                                            paddingLeft='3px' />
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Nombre(s)
                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label=''
                                            name='nombre'
                                            placeHolder='Nombre(s)'
                                            value={nombreDP.estatus ? nombreDP.value : (edit != null ? edit.v_nombre : '')}
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage=' Nombre(s) con caracteres inválidos'
                                            tooltipMessage=' Nombre(s)'
                                            paddingLeft='3px' />
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Clave de elector
                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label='' name='claveElector'
                                            placeHolder='AAAAAA12345678A123'
                                            value={claveElectorDP.estatus ? claveElectorDP.value : (edit != null ? edit.v_claveElector : '')}
                                            disabled={true}
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Clave de elector con caracteres inválidos'
                                            tooltipMessage='Clave de elector'
                                            paddingLeft='3px' />
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>CURP
                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label='' name='curp'
                                            placeHolder='AAAA123456AAABBB01'
                                            value={curpDP.estatus ? curpDP.value : (edit != null ? edit.v_curp : '')}
                                            type='text'
                                            regex={/^([a-zA-Z]{4}\d{6}[a-zA-Z]{6}\d{2})$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='CURP con caracteres inválidos'
                                            tooltipMessage='CURP'
                                            paddingLeft='3px' />
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <div>
                                            <font color="#d5007f">*</font>Genero
                        <br />
                                            <Radio.Group onChange={changeGenero} value={generoDP.estatus ? generoDP.value : (edit != null ? edit.v_genero : '')}
                                            >
                                                <Radio value={"M"}>Femenino</Radio>
                                                <Radio value={"H"}>Masculino</Radio>

                                            </Radio.Group>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-5 col-sm-12">
                                        <div>
                                            <font color="#d5007f">*</font>Fecha de nacimiento
                        <br />
                                            <DatePicker placeHolder={"DD/MM/AAAA"} value={fechaNacimientoDP.estatus ?
                                                moment(fechaNacimientoDP.value, "DD-MM-YYYY") :
                                                (edit != null ? (moment((new Date(edit.v_fechaNacimiento)), "DD-MM-YYYY")) : '')}
                                                format={'DD/MM/YYYY'}
                                                onChange={(evento, fecha) => changeFechadeNacimiento(evento, fecha)} />
                                        </div>
                                    </div>
                                    <div className="col-md-7 col-sm-12">
                                        <div>
                                            <font color="#d5007f">*</font>Ciudadana/o mexicana/o
                        <br />
                                            <Radio.Group onChange={changeCiudanania} value={ciudadanaDP.estatus ? ciudadanaDP.value : (edit != null ? edit.v_ciudadania : '')} >
                                                <Radio value={1}>Nacida/o en territorio mexicano</Radio>
                                                <Radio value={2}>Nacida/o en el extranjero</Radio>
                                                <Radio value={3}>Nacionalizada/o</Radio>
                                            </Radio.Group>
                                        </div>
                                    </div>

                                </div>


                                <div className="row">
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Entidad Federativa de nacimiento
                        <br />
                                        <Select style={{ width: '100%' }} onChange={changeEntidad} value={entidadDP.estatus ? entidadDP.value : (edit != null ? edit.v_entidad : "Selecciona una opción")}
                                            placeholder="Selecciona una opción">
                                            {
                                                Estados != null ?
                                                    Estados.estados.map((tem) => (
                                                        <Select.Option value={tem.idEstado}>{tem.nombreEstado}</Select.Option>
                                                    ))
                                                    : null

                                            }
                                        </Select>
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Municipio de nacimiento
                        <br />
                                        <Select onChange={changeMunicipio} style={{ width: '100%' }} value={municipioDP.estatus ? municipioDP.value : (edit != null ? edit.v_municipio : "Selecciona una opción")}
                                            placeholder="Selecciona una opción">
                                            {
                                                llenarDistrito != null ?
                                                    Estados.estados[(llenarDistrito - 1)].distritos.map((tem) => (
                                                        <Select.Option value={tem.idDistrito}>{tem.nombreDistrito}</Select.Option>
                                                    ))
                                                    : null
                                            }

                                            {
                                                municipioDP.estatus ? null : (edit != null ?
                                                    Estados.estados[(edit.v_municipio - 1)].distritos.map((tem) => (
                                                        <Select.Option value={tem.idDistrito}>{tem.nombreDistrito}</Select.Option>
                                                    ))
                                                    : null)
                                            }
                                        </Select>
                                    </div>
                                    <div className="col-md-4 col-sm-12">

                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Profesión
                        <br />
                                        <Select onChange={changeProfesion}
                                            style={{ width: '100%' }} value={profesionDP.estatus ? profesionDP.value : (edit != null ? edit.v_profesion : "Selecciona una opción")}
                                            placeholder="Selecciona una opción">
                                            {

                                                tempProfesiones != null ?
                                                    tempProfesiones.map((tem) => (
                                                        <Select.Option value={tem.idProfesion}>{tem.descripcion}</Select.Option>
                                                    ))
                                                    : null
                                            }
                                        </Select>
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Tratamiento
                        <br />
                                        <Select onChange={changeTratamiento}
                                            style={{ width: '100%' }} value={tratamientoDP.estatus ? tratamientoDP.value : (edit != null ? edit.v_tratamiento : "Selecciona una opción")}
                                            placeholder="Selecciona una opción">
                                            {
                                                tempTratamiento != null ?
                                                    tempTratamiento.map((tem) => (
                                                        <Select.Option value={tem.idTratamiento}>{tem.descripcion}</Select.Option>
                                                    ))
                                                    : null

                                            }
                                        </Select>
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label='Puesto que ejerce' name='puestoEjerce'
                                            placeHolder='Ingresa información'

                                            value={puestoDP.estatus ? puestoDP.value : (edit != null ? edit.p_puestoEjerce : '')}
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Puesto que ejerce con caracteres inválidos'
                                            tooltipMessage='Puesto que ejerce'
                                            paddingLeft='3px' />
                                    </div>
                                </div>

                            </div>
                        }

                        {/*
            Contacto 
          */}
                        {numeroPagina.pagina === 1 &&
                            <div style={{ paddingTop: "30px" }} className="steps-content" >
                                <br />
                                <div class="form-row mt-4">
                                    <div class="col-sm-4 pb-3">
                                        <font color="#d5007f">*</font>Calle
                        <InputINE className='ant-input-sinIconoIzquierdo' onSelectLanguage={handleInput}
                                            label='' name='calle'
                                            placeHolder='Ingresa calle'
                                            value={calleC.estatus ? calleC.value : (edit != null ? edit.d_calleContacto : 'Selecciona una opción')}
                                            type='text'
                                            regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Calle con caracteres inválidos'
                                            tooltipMessage='Calle'
                                            paddingLeft='3px' />
                                    </div>
                                    <div class="col-sm-2 pb-3">
                                        {/* {console.log("quitar ", checkBoxNum)} */}
                                        {sinNumeroC.estatus ?
                                            <div>
                                                No. exterior
                        <p className='h45'>S/N</p>
                                            </div>
                                            :
                                            <>
                                                <font color="#d5007f">*</font>No. exterior
                            <InputINE onSelectLanguage={handleInput}
                                                    label='' name='numeroExt'
                                                    placeHolder='Número ext.'
                                                    value={numeroExteriorC.estatus ? numeroExteriorC.value : (edit != null ? edit.d_numeroExterior : null)}
                                                    type='text'
                                                    regex={/^\d+$/}
                                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                    warningMessage='Número exterior sólo debe contener números'
                                                    tooltipMessage='Número exterior'
                                                    paddingLeft='3px'
                                                />
                                            </>
                                        }
                                    </div>
                                    <div class="col-sm-2 pb-3">
                                        {sinNumeroC.estatus ?
                                            <div>
                                                No. interior
                        <p className='h45'>S/N</p>
                                            </div>
                                            :
                                            <InputINE onSelectLanguage={handleInput}
                                                label='No. interior' name='numeroInt'
                                                placeHolder='Número int.'
                                                value={numeroInteriorC.estatus ? numeroInteriorC.value : (edit != null ? edit.d_numeroInterior : '')}
                                                type='text'
                                                regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}

                                                // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                warningMessage='Número interior sólo debe contener números'
                                                tooltipMessage='Número interior'
                                                paddingLeft='3px'
                                            />
                                        }
                                    </div>
                                    {/* checked={numeroExterior==-1&&numeroInterior==-1?true:false} */}
                                    <div class="col-sm-2 pb-3">
                                        <Checkbox style={{ paddingTop: '36px' }} onChange={onChangeCheckBoxNum}>Sin número</Checkbox>

                                    </div>
                                    <div class="col-sm-2 pb-3">
                                        <font color="#d5007f">*</font>C.P.
                        <InputINE onSelectLanguage={handleInput}
                                            onChange={onChangeCodigoPostal}
                                            label='' name='codigoPostal'
                                            placeHolder='12345'
                                            value={codigoPostalC.estatus ? codigoPostalC.value : (edit != null ? edit.d_codigoPostalContacto : '')}
                                            type='text'
                                            regex={/^\d{5}$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Código Postal sólo debe contener números'
                                            tooltipMessage='Código Postal'
                                            paddingLeft='3px'
                                            number='number'
                                            maxLength={5}
                                            style={{ disabled: true }}
                                        />
                                    </div>
                                </div>

                                <div class="form-row mt-4">
                                    <div class="col-sm-4 pb-3">
                                        {apiCodigo ?
                                            <div>
                                                <font color="#d5007f">*</font>Entidad
                    <p className='h45'>{entidadFederativaC.value}</p>
                                            </div>
                                            :
                                            <>
                                                <font color="#d5007f">*</font>Entidad
                            <InputINE onSelectLanguage={handleInput}
                                                    label='' name='entidad'
                                                    placeHolder='Ingresa Entidad'
                                                    type='text'
                                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                                    value={entidadFederativaC.estatus ? entidadFederativaC.value : (edit != null ? edit.d_entidadContacto : '')}
                                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                    warningMessage='Entidad tiene caracteres inválidos'
                                                    tooltipMessage='Entidad '
                                                    paddingLeft='3px' />
                                            </>
                                        }
                                    </div>

                                    <div class="col-sm-4 pb-2">
                                        {apiCodigo ?
                                            <div>
                                                <font color="#d5007f">*</font>Municipio
                                <p className='h45'>{municipioC.value}</p>
                                            </div>
                                            :
                                            <>
                                                <font color="#d5007f">*</font>Municipio
                            <InputINE onSelectLanguage={handleInput}
                                                    label='' name='municipio'
                                                    placeHolder='Ingresa Municipio'
                                                    type='text'
                                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                                    // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                                    value={municipioC.estatus ? municipioC.value : (edit != null ? edit.d_municipioContacto : '')}
                                                    warningMessage='Municipio tiene caracteres inválidos'
                                                    tooltipMessage='Municipio'
                                                    paddingLeft='3px' >
                                                </InputINE>
                                            </>
                                        }
                                    </div>
                                    <div class="col-sm-4 pb-2">
                                        <font color="#d5007f">*</font>Colonia
                    {
                                            apiCodigo ?

                                                <Select style={{ width: '100%' }} onChange={selectColonia} value={coloniaC.estatus ? coloniaC.value : ''} >
                                                    {colonias.map(tem =>

                                                        <Select.Option value={tem}>{tem}</Select.Option>

                                                    )}
                                                </Select>
                                                :
                                                <InputINE onSelectLanguage={handleInput}
                                                    label='' name='colonia'
                                                    placeHolder='Ingresa tu colonia'
                                                    type='text'
                                                    value={coloniaC.estatus ? coloniaC.value : (edit != null ? edit.d_coloniaContacto : 'Selecciona una opción')}
                                                    regex={/^([\w. áéíóúÁÉÍÓÚñ.,-;#’/])+$/}
                                                    warningMessage='Colonia tiene caracteres inválidos'
                                                    tooltipMessage='Colonia'
                                                    paddingLeft='3px' />
                                        }

                                    </div>
                                </div>

                                <h3>Contacto</h3>
                                <div class="form-row mt-4">
                                    <div class="col-sm-4 pb-3">
                                        <font color="#d5007f">*</font>Correo
                        <InputINE onSelectLanguage={handleInput}
                                            label='' name='correo'
                                            placeHolder='nombre@dominio.com'
                                            value={correoC.estatus ? correoC.value : (edit != null ? edit.v_correoContacto : '')}
                                            type='text'
                                            regex={/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/}
                                            // icon={<LockOutlined style={{ color: '#d5007f' }} />}
                                            warningMessage='Correo tiene caracteres inválidos'
                                            tooltipMessage='Correo'
                                            paddingLeft='3px' />
                                    </div>
                                    <div class="col-sm-4 pb-3">
                                        <font color="#d5007f">*</font>Teléfono
                        {
                                            telefonoC.estatus ?
                                                <Telefonos getData={changeTelefono} lista={telefonoC.value} />
                                                :
                                                (edit != null ?
                                                    <Telefonos getData={changeTelefono} lista={edit.telefonos} />

                                                    :
                                                    <h1>se esta cargado el sistema</h1>

                                                )

                                            // <Telefonos getData={changeTelefono} lista={edit.telefonos}  />

                                        }
                                    </div>
                                </div>
                                <div><p /><p /><p /></div>
                            </div>
                        }

                        {/*
            Estatus
          */}
                        {numeroPagina.pagina === 2 &&
                            <div style={{ paddingTop: "40px" }} className="steps-content">
                                <br />
                                <div className="row">
                                    <br></br>
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Fecha de ingreso
                              <br />
                                        <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} value={fechaIngresoEs.estatus ?
                                            moment(fechaIngresoEs.value, "DD-MM-YYYY") : (edit != null && edit.p_fechaIngreso != null ? (moment((new Date(edit.p_fechaIngreso)), "DD-MM-YYYY")) : null)}
                                            onChange={(evento, fecha) => changeFechaIngreso(evento, fecha)} />
                                    </div>
                                    <div className="col-md-4 col-sm-12">
                                        Fecha de nombramiento
                              <br />
                                        <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} value={fechaNombramientoEs.estatus ?
                                            (fechaNombramientoEs.value != null ? moment(fechaNombramientoEs.value, "DD-MM-YYYY") : '') : (edit != null && edit.p_fechaNombramiento != null ? (moment((new Date(edit.p_fechaNombramiento)), "DD-MM-YYYY")) : '')}
                                            onChange={(evento, fecha) => changeFechaNombramiento(evento, fecha)} />
                                    </div>
                                    <div>
                                        Fecha de titularidad
                              <br />
                                        <DatePicker placeHolder={"DD/MM/AAAA"} format={'DD/MM/YYYY'} value={fechaTitularidadEs.estatus ?
                                            (fechaTitularidadEs.value != null ? moment(fechaTitularidadEs.value, "DD-MM-YYYY") : '') : (edit != null && edit.p_fechaTitulariad != null ? (moment((new Date(edit.p_fechaTitulariad)), "DD-MM-YYYY")) : '')}
                                            onChange={(evento, fecha) => changeFechaTitulariad(evento, fecha)} />
                                    </div>

                                </div>
                                <div className="row">
                                    <div className="col-md-4 col-sm-12">
                                        <font color="#d5007f">*</font>Nombramiento
                          <br />
                                        <CargarArchivo editar={urlArchvivo} getData={changeArchivo} ponerArchivo={nombramientoEs.value} />
                                    </div>
                                    <div className="col-md-8 col-sm-12">
                                        <font color="#d5007f">*</font>Puesto
                              <br />
                                        <Radio.Group onChange={changePuesto} value={puestoEs.estatus ? puestoEs.value : (edit != null ? edit.p_tipoPuesto : '')}>
                                            <Radio value={1}>Propietario</Radio>
                                            <Radio value={2}>Encargado del Despacho</Radio>
                                        </Radio.Group>
                                    </div>
                                    <div className="col-md-12 col-sm-12">
                                        Captura firma
                              <br />
                                        <CargarImagen getData={changeFirma} texto={"Firma"} ponerImagen={firmaEs.value} editar={urlFirma} />
                                    </div>
                                </div>
                            </div>
                        }

                        {/*
            experiencia 
          */}
                        {numeroPagina.pagina === 3 &&
                            <div style={{ paddingTop: "60px" }} className="steps-content">
                                <div >
                                    {
                                        edit != null ?
                                            <Experiencia puestos={listaPuestos} getDataSend={changeExperienciaSend}
                                                getData={changeExperiencia}
                                                listExperienciaSend={experienciaSend.value}
                                                listExperiencia={experiencia.value} modifcar={experienciaSend.estatus}
                                                mostrar={{ mostrar: addkeyExperienciaMostrar(edit.experiencia), enviar: addkeyExperienciaEnviar(edit.experiencia) }} />

                                            : null
                                    }


                                </div>

                            </div>
                        }


                        <div className="steps-action row espacios">

                            {numeroPagina.pagina == 0 &&
                                <div className="col-md-12 col-sm-6">

                                    <u className="letras float-right" onClick={next}>Siguiente <i className="iconSigueinte" /></u>
                                </div>
                            }
                            {numeroPagina.pagina < 3 && numeroPagina.pagina > 0 &&
                                <>
                                    <div className="col-md-6 col-sm-6">
                                        <u className="letras" onClick={prev}><i className="iconAtras" />Atras </u>
                                    </div>
                                    <div className="col-md-6 col-sm-6">

                                        <u className="letras float-right" onClick={next}>Siguiente <i className="iconSigueinte" /></u>
                                    </div>
                                </>
                            }

                            {
                                numeroPagina.pagina === 3 &&
                                <>
                                    <div className="col-md-6 col-sm-6">
                                        <u className="letras" onClick={prev}><i className="iconAtras" />Atras </u>
                                    </div>
                                    <div className="col-md-6 col-sm-12 ">
                                        {
                                            edit != null ?
                                                <Button type="primary" onClick={enviar}>
                                                    Guardar registro
                        </Button>
                                                : null
                                        }
                                    </div>
                                </>
                            }

                        </div>

                        <Dialog open={guardar} >
                            <DialogContent>Se Guardo con exito   </DialogContent>
                            <DialogActions>
                                <Link to={`/vocales/home`}>
                                    <Button >Aceptar</Button>
                                </Link>
                            </DialogActions>
                        </Dialog>
                    </div>

                </div>

            }


        </>

    )
}




