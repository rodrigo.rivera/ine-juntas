import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { FileAddOutlined, FormOutlined, CodeSandboxCircleFilled } from '@ant-design/icons';
import { Table, Button, List, Popconfirm, Spin, Modal, Layout, Icon } from 'antd';
import moment from "moment";
import 'antd/dist/antd.css';

import Image from 'react-bootstrap/Image'
import axios from "axios";
//import Menu from "../Componets/Sider/Sider";

import jwtDecode from "jwt-decode";
import VocalesFormulario from "./VocalesFormulario";
const { Content } = Layout;
const { Column, ColumnGroup } = Table;
const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;
export default function HomeVocales(props) {

    const [dataMenu] = useState(props.menu)

    const [lista, setLista] = useState(null)
    const [bitacora, setBitacora] = useState(null);

    const [urlImagen01, setUrlImagen01] = useState(null);
    const [urlImagen02, setUrlImagen02] = useState(null);
    const [urlImagen03, setUrlImagen03] = useState(null);
    const [urlImagen04, setUrlImagen04] = useState(null);
    const [urlImagen05, setUrlImagen05] = useState(null);
    const [urlImagen06, setUrlImagen06] = useState(null);
    const [tipoModal, setTipoModal] = useState(null);
    const [roles, setRoles] = useState(null);
    /**
     * estados modal 
     */



    const [idPuesto, setidPuesto] = useState(null);
    const [incompeltoId, setincompeltoId] = useState(null);
    const [incompeltoCargo, setincompeltoCargo] = useState(null);
    const [incompeltoNombre, setincompeltoNombre] = useState(null);
    const [incompeltoFechaIngreso, setincompeltoFechaIngreso] = useState(null);
    const [incompeltoFechaNombramiento, setincompeltoFechaNombramiento] = useState(null);
    const [incompeltoFechaTerminoCargo, setincompeltoFechaTerminoCargo] = useState(null);
    const [incompeltoMotivo, setincompeltoMotivo] = useState(null);
    const [sustituidoId, setsustituidoId] = useState(null);
    const [sustituidoCargo, setsustituidoCargo] = useState(null);
    const [sustituidoNombre, setsustituidoNombre] = useState(null);
    const [sustituidoFechaIngreso, setsustituidoFechaIngreso] = useState(null);
    const [sustituidoFechaNombramiento, setsustituidoFechaNombramiento] = useState(null);
    const [sustituidoFechaTemrinoCargo, setsustituidoFechaTemrinoCargo] = useState(null);
    const [sustituidoMotivo, setsustituidoMotivo] = useState(null);
    const [sustitutoId, setsustitutoId] = useState(null);
    const [sustitutoCargo, setsustitutoCargo] = useState(null);
    const [sustitutoNombre, setsustitutoNombre] = useState(null);
    const [sustitutoFechaIngreso, setsustitutoFechaIngreso] = useState(null);
    const [sustitutoFechaNombramiento, setsustitutoFechaNombramiento] = useState(null);
    const [evidencia01, setEvidencia01] = useState(null)
    const [evidencia02, setEvidencia02] = useState(null)
    const [nombremiento, setNombramiento] = useState(null)


    const columnas = [
        {
            title: "Nombre",
            dataIndex: "nombre",
            width: "15%"
        },
        {
            title: "Cargo",
            dataIndex: "cargo",
            width: "15%"

        }, {
            title: "Movimiento",
            dataIndex: "movimiento",
            width: "15%"
        },
        {
            title: "Responsable del Movimiento",
            dataIndex: "responsableMov",
            width: "15%"
        },
        {
            title: "Fecha",
            dataIndex: "fechaHora",
            width: "15%"
        },
        {
            title: "Accion",
            dataIndex: "evidencia",
            width: "15%",
            render: (text, record) =>
                bitacora.length >= 1 ? (
                    <>

                        {
                            record.estatus !== 1 ? <>
                                <Button
                                    key={JSON.stringify(record.idVocal)}
                                    shape="circle"
                                    className=".ant-btn ant-btn-modifica"
                                    style={{ marginLeft: '15px' }}
                                    onClick={() => mostarModal(record)}
                                >
                                    <FormOutlined className="iconMostrar" size={20} />
                                </Button>
                            </>
                                : null
                        }


                        {/* {JSON.stringify(record)} */}
                        {/* {"idVocal:" + record.idVocal + "---idpuesto_" + record.idDPuestoVocal + "----  estatus:" + record.estatus} */}


                        <Link to={{
                            pathname: `/vocales/${record.idVocal}/mostrar`
                        }
                        } >

                        </Link>



                    </>
                ) : null
        }
    ]

    const [mostrar, setMostrar] = useState(false);



    const mostarModal = (data) => {

        console.log("se mmuestra  el modal ", data)
        setTipoModal(data)
        setMostrar(true);

    }

    useEffect(() => {
        //console.log(dataMenu.infoMenu);

        if (dataMenu.infoMenu.distritoFedSelec != null && dataMenu.infoMenu.estadoSelec) {


            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
                .then((response) => {
                    // console.log("urls::::::::::");
                    //console.log(response);
                    localStorage.setItem('aplicaciones', response.data.urlLogin);
                    if (response.status === 200) {
                        //console.log("me voy a trarr ", localStorage.getItem("idDistrito"))
                        let data = {
                            //"idDetalleProceso": 106,
                            "idDistrito": dataMenu.infoMenu.distritoFedSelec.idDistrito,
                            "idEstado": dataMenu.infoMenu.estadoSelec.idEstado,
                        }


                        axios.post(`${response.data.centralSesiones}/vocales/consulta`, data).then((respuesta) => {
                            //localStorage.removeItem("idDistrito")
                            localStorage.setItem("stop", true);
                            console.log("...................exito", respuesta.data)


                            setRoles(dataMenu.infoMenu.listMenu[0].subMenus[0].modulos[0].listAccionesModulo);


                            setBitacora(formatoBitacora(respuesta.data.bitacora));

                            //setBitacora(respuesta.data.bitacora);
                            setLista(respuesta.data.vocalesHome);
                            // trae imagenes 
                            if (respuesta.data.vocalesHome != null) {
                                let i = 0;
                                respuesta.data.vocalesHome.forEach(temp => {
                                    // inicio 

                                    let data = {
                                        idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                                        idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                                        modulo: "Vocales",
                                        idSubModulo: temp.id,
                                        nombreSistema: "junta",
                                        modificar: true,
                                    }
                                    console.log("data que va a traer imagenes  ", data)
                                    let token = localStorage.getItem('accessToken');
                                    axios.post(`${response.data.centralSesiones}/vocales/descargaArchivo`, data, {
                                        headers: {
                                            Authorization: token,
                                        },
                                    })
                                        .then((respuesta) => {
                                            //localStorage.setItem("accessToken", response.data.token);
                                            console.log("la repuesta img ", respuesta.data)
                                            let lista = base64ToBlobs(respuesta.data.archivo)
                                            //console.log(lista.blobs);


                                            let x = { id: temp.id, url: lista.blobs[0].source }

                                            if (i === 0) {
                                                setUrlImagen01(x)
                                            }
                                            if (i === 1) {
                                                setUrlImagen02(x);
                                            }
                                            if (i === 2) {
                                                setUrlImagen03(x)
                                            }
                                            if (i === 3) {
                                                setUrlImagen04(x);
                                            }
                                            if (i === 4) {
                                                setUrlImagen04(x)
                                            }
                                            if (i === 5) {
                                                setUrlImagen05(x);
                                            }
                                            if (i === 6) {
                                                setUrlImagen06(x);
                                            }

                                            i++;
                                        })
                                        .catch((erorr) => {
                                            console.log('eroror ', erorr);
                                        });



                                    //fin 
                                });

                                for (let index = 0; index < 5; index++) {
                                    urlImagen03.push({ id: index });

                                }
                            }

                        }).catch((erorr) => {
                            // console.log("eroror ", erorr)
                        });
                    }
                });

        }
        //setTest("valeria")

    }, []);


    const formatoBitacora = (bitacora) => {
        console.log("la bitacora", bitacora);
        let temp = []
        bitacora.forEach(t => {

            if (t.movimiento != null) {
                let data = {
                    cargo: t.cargo,
                    estatus: t.estatus,
                    evidencia: t.evidencia,
                    fechaHora: moment(new Date(t.fechaHora)).format("DD/MM/YYYY"),
                    idDPuestoVocal: t.idDPuestoVocal,
                    idDistrito: t.idDistrito,
                    idEstado: t.idEstado,
                    idVocal: t.idVocal,
                    movimiento: t.movimiento,
                    nombre: t.nombre,
                    responsableMov: t.responsableMov,
                }
                temp.push(data);
            }
        });
        console.log("Salida de formato Bitacora ", temp)
        return temp;
    }

    const base64ToBlobs = (base64Images) => {
        //console.log("recibe ", base64Images)
        var images = { blobs: [] }

        for (var i = 0; i < base64Images.length; i++) {
            //console.log(base64Images[i].tipoArchivo)
            if (base64Images[i].archivoBase64 !== null) {
                images.blobs.push({
                    "source": b64toBlob(base64Images[i].archivoBase64, base64Images[i].tipoArchivo),
                    "name": base64Images[i].nombreArchivo.split('-')[0],
                    "order": base64Images[i].nombreArchivo.split('-')[1]
                });
            }

        }
        //console.log("Archivos:::::::::::::::::::::::")
        //console.log(images)
        return images
    };

    function b64toBlob(b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        b64Data = b64Data.replace(/^[^,]+,/, '');
        b64Data = b64Data.replace(/\s/g, '');
        var byteCharacters = window.atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = URL.createObjectURL(new Blob(byteArrays, { type: contentType }));
        return blob;
    }

    const buscarImg = id => {
        //console.log("el valor de el id", id)
        if (urlImagen01 != null) {
            //console.log("tamaño ", urlImagen01)
            //console.log("tamaño ", urlImagen01.id, "==", id)

            if (urlImagen01.id == id) {
                return urlImagen01.url;
            }

            if (urlImagen02 != null && urlImagen02.id == id) {
                return urlImagen02.url;
            }
            if (urlImagen03 != null && urlImagen03.id == id) {
                return urlImagen03.url;
            }
            if (urlImagen04 != null && urlImagen04.id == id) {
                return urlImagen04.url;
            }
            if (urlImagen05 != null && urlImagen05.id == id) {
                return urlImagen05.url;
            }
            if (urlImagen06 != null && urlImagen06.id == id) {
                return urlImagen06.url;
            }
        }

        return "https://www.w3schools.com/css/paris.jpg";
    }

    const getDataSustitucion = (status, idPuestoVocal) => {

        return new Promise((resolve, reject) => {

            axios.get(window.location.origin + '/JsonHelpers/jsonUrl.json').then((response) => {
                // console.log("urls::::::::::");

                let token = localStorage.getItem('accessToken');
                //console.log('el token para insertar DomiciliosDeJunta: ', token);
                if (response.status === 200) {
                    //setUrls(response.data);
                    //console.log("status: ", status, "\n dPuestoVocal:", idPuestoVocal)

                    const peticionData = {

                        "newToken": "",
                        "idEstado": 4,
                        "idDistrito": 2,
                        "idPuestoVocal": idPuestoVocal,
                        "idSustitucion": 41,
                        "idBajaVocal": 64,
                        "status": status
                    }

                    axios
                        .post(`${response.data.centralSesiones}/vocales/dataSustitucion`,
                            peticionData
                            , {
                                headers: {
                                    Authorization: token,
                                },
                            })
                        .then((respuesta) => {
                            //localStorage.setItem("accessToken", response.data.token);7

                            //console.log("respuesta datos ", respuesta)
                            if (respuesta.data.idPuesto != null) {

                                descargaEvidencias(respuesta.data.idPuesto).then((respues) => {
                                    //console.log("evidencias ", respues)
                                    //console.log("tamaño de la ", tipoModal.estatus)

                                    if (tipoModal.estatus == 3) {// muestra nombreminto 
                                        //console.log("cambio", respues.blobs[0])
                                        //setEvidencia01(respues.blobs[0]);
                                        localStorage.setItem("evidencia01", respues.blobs[0].source)
                                        descargaNombrmiento(respuesta.data.incompeltoId).then((res)=>{
                                            console.log("respuesta nom ",res)
                                            localStorage.setItem("nombramiento",res.blobs[1].source)
                                            console.log("respuesta nom ",localStorage.getItem("nombramiento"))
                                        });
                                    } else {
                                        localStorage.setItem("evidencia02", respues.blobs[0].source)
                                        localStorage.setItem("evidencia01", respues.blobs[1].source)
                                    }



                                    resolve(respuesta);
                                })
                            } else {
                                resolve(respuesta);

                            }



                        })
                        .catch((error) => {
                            console.log('eroror ', error);
                            //alertINE("Ocurrio un error en el servicio  Clave electro")
                            reject({ err: true, data: "ocurrio errro " + error });
                        });
                }
            });

        });

    }

    const descargaEvidencias = (id) => {
        //console.log("entro con ", id)
        return new Promise((resolve, reject) => {

            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
                .then((response) => {
                    // console.log("urls::::::::::");
                    console.log(response);
                    let token = localStorage.getItem('accessToken');
                    if (response.status === 200) {
                        //setUrls(response.data);


                        // imagenes 

                        let data = {
                            idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                            idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                            modulo: "VocalesSustituciones",
                            idSubModulo: id,
                            nombreSistema: "junta",
                            modificar: true,
                        }
                        //console.log("Mostrar datos ", data)
                        axios.post(`${response.data.centralSesiones}/vocales/descargaArchivoSustitucion`, data, {
                            headers: {
                                Authorization: token,
                            },
                        })
                            .then((respuesta) => {
                                //localStorage.setItem("accessToken", response.data.token);
                                // console.log("la data ", respuesta)
                                let lista = base64ToBlobs(respuesta.data.archivo)
                                //console.log(lista.blobs);
                                //setEvidencia01(lista.blobs[0].source);
                                //setEvidencia01(lista.blobs[1].source);
                                resolve(lista);
                            })
                            .catch((erorr) => {
                                console.log('eroror ', erorr);
                                reject("ocurrio un error " + erorr)
                            });
                    }
                });

        });





    }

    const descargaNombrmiento = (id) => {
        console.log("entro con ",id)
        return new Promise((resolve, reject) => {

            axios.get(window.location.origin + "/JsonHelpers/jsonUrl.json")
                .then((response) => {
                    // console.log("urls::::::::::");
                    //console.log(response);
                    let token = localStorage.getItem('accessToken');
                    if (response.status === 200) {
                        //setUrls(response.data);


                        // imagenes 

                        let data = {
                            idEstado: dataMenu.infoMenu.estadoSelec.idEstado,
                            idDistrito: dataMenu.infoMenu.distritoFedSelec.idDistrito,
                            modulo: "Vocales",
                            idSubModulo: id,
                            nombreSistema: "junta",
                            modificar: true,
                        }
                        console.log("Mostrar datos ", data)
                        axios.post(`${response.data.centralSesiones}/vocales/descargaArchivo`, data, {
                            headers: {
                                Authorization: token,
                            },
                        })
                            .then((respuesta) => {
                                //localStorage.setItem("accessToken", response.data.token);
                                console.log("la data ", respuesta)
                                let lista = base64ToBlobs(respuesta.data.archivo)
                                // console.log(lista.blobs);

                                resolve(lista);
                                // setNombramiento(lista.blobs[1].source);


                            })
                            .catch((error) => {
                                console.log('eroror ', error);
                                reject(error)
                            });



                    }
                });
        });




    }

    const cerrarModal = () => {
        setMostrar(false);
        localStorage.removeItem("envidencia01");
        localStorage.removeItem("envidencia02");
        localStorage.removeItem("nombramiento")
    }

    const tipoSustitucion = (tipo) => {
        //console.log("Entra tipo ", tipo)
        // incompleto 
        if (tipo != null) {
            console.log("entro ")
            getDataSustitucion(tipo.estatus, tipo.idDPuestoVocal).then((res) => {
                //console.log("exito", res.data);
                setidPuesto(res.data.idPuesto)
                setincompeltoId(res.data.incompeltoId);
                setincompeltoCargo(res.data.incompeltoCargo);
                setincompeltoNombre(res.data.incompeltoNombre);
                setincompeltoFechaIngreso(getFecha(res.data.incompeltoFechaIngreso));
                setincompeltoFechaNombramiento(getFecha(res.data.incompeltoFechaNombramiento));
                setincompeltoFechaTerminoCargo(getFecha(res.data.incompeltoFechaTerminoCargo));
                setincompeltoMotivo(res.data.incompeltoMotivo);
                setsustituidoId(res.data.sustituidoId);
                setsustituidoCargo(res.data.sustituidoCargo);
                setsustituidoNombre(res.data.sustituidoNombre);
                setsustituidoFechaIngreso(getFecha(res.data.sustituidoFechaIngreso));
                setsustituidoFechaNombramiento(getFecha(res.data.sustituidoFechaNombramiento));
                setsustituidoFechaTemrinoCargo(getFecha(res.data.sustituidoFechaTemrinoCargo));
                setsustituidoMotivo(res.data.sustituidoMotivo);
                setsustitutoId(res.data.sustitutoId);
                setsustitutoCargo(res.data.sustitutoCargo);
                setsustitutoNombre(res.data.sustitutoNombre);
                setsustitutoFechaIngreso(getFecha(res.data.sustitutoFechaIngreso));
                setsustitutoFechaNombramiento(getFecha(res.data.sustitutoFechaNombramiento));



            }).catch((error) => {
                console.log("error promesa ", error)
            });

            if (tipo.estatus === 3) {


                return (
                    <>
                        <h1 style={{
                            "font-size": "24px",
                            'color': '#e149a4'
                        }}>Vocal sustituido</h1>
                        <div className="row" >
                            <div className="col-md-6">
                                <label className="modal-mostar">{incompeltoCargo} </label>
                                <p>{incompeltoNombre} </p>
                                <label className="modal-mostar">Fecha nombramiento: </label>
                                <p>{incompeltoFechaNombramiento}</p>
                            </div>
                            <div className="col-md-6">
                                <label className="modal-mostar">Fecha de ingreso</label>
                                <p>{incompeltoFechaIngreso}</p>
                                <div className="col-md-6">
                                    <label className="modal-mostar">Nombremiento:</label>
                                    <br></br>
                                    {localStorage.getItem("nombramiento") != null ? <a href={localStorage.getItem("nombramiento")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Nombramiento.pdf</a> </a> : "cargando..."}

                                </div>
                            </div>
                        </div>
                        <br />
                        <h3 style={{
                            'color': '#e149a4'
                        }}>Término en el cargo</h3>
                        <div className="row" >
                            <div className="col-md-6">
                                <label className="modal-mostar">Fecha de término en el cargo:</label>
                                <p>{incompeltoFechaTerminoCargo} </p>
                                <label className="modal-mostar">Motivo: </label>
                                <p>{incompeltoMotivo}</p>
                            </div>
                            <div className="col-md-6">
                                <label className="modal-mostar">&nbsp;&nbsp;</label>
                                <p>&nbsp;&nbsp;</p>
                                <label className="modal-mostar">Evidencia: </label>
                                <br></br>
                                {localStorage.getItem("evidencia01") != null ? <a style={{ 'color': '#e149a4' }} href={localStorage.getItem("evidencia01")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Evidencia.pdf</a> </a> : "hay"}
                            </div>

                        </div>
                    </>

                )

            }

            // sustitucion
            if (tipo.estatus == 4) {

                return (
                    <div className="row">
                        <div className="col-md-6">
                            <h1 style={{
                                "font-size": "24px",
                                'color': '#e149a4'
                            }}>Vocal sustituido</h1>
                            <label className="modal-mostar">{sustituidoCargo}</label>
                            <p>{sustituidoNombre}</p>
                            <label className="modal-mostar">Fecha de Ingreso:</label>
                            <p>{sustituidoFechaIngreso}</p>

                            <label className="modal-mostar">Fecha nombramiento:</label>
                            <p>{sustituidoFechaNombramiento}</p>

                            <label className="modal-mostar">nombramiento:</label>
                            <br></br>
                            {localStorage.getItem("nombramiento") != null ? <a style={{ 'color': '#e149a4' }} href={localStorage.getItem("nombramiento")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Nombramiento.pdf</a> </a> : "cargando..."}
                            <br></br>
                            <br></br>
                            <h6 style={{ 'color': '#e149a4' }}>Término en el cargo</h6>

                            <br></br>
                            <label className="modal-mostar">fecha de término en el cargo:</label>
                            <p>{sustituidoFechaTemrinoCargo}</p>
                            <label className="modal-mostar">Motivo:</label>
                            <p>{sustituidoMotivo}</p>
                            <label className="modal-mostar">Evidencia:</label>
                            <br></br>
                            {localStorage.getItem("evidencia01") != null ? <a style={{ 'color': '#e149a4' }} href={localStorage.getItem("evidencia01")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Evidencia.pdf</a> </a> : null}
                        </div>
                        <div className="col-md-6">
                            <h1 style={{
                                "font-size": "24px",
                                'color': '#e149a4'
                            }}>Vocal sustituto</h1>

                            <label className="modal-mostar">{sustitutoCargo}</label>
                            <p>{sustitutoNombre}</p>
                            <label className="modal-mostar">Fecha de Ingreso (nuevo cargo):</label>
                            <p>{sustitutoFechaIngreso}</p>

                            <label className="modal-mostar">Fecha nombramiento (nuevo cargo):</label>
                            <p>{sustitutoFechaNombramiento}</p>

                            <label className="modal-mostar">Evidencia</label>
                            <br></br>
                            {localStorage.getItem("evidencia02") != null ? <a style={{ 'color': '#e149a4' }} href={localStorage.getItem("evidencia02")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Evidencia.pdf</a> </a> : null}

                        </div>
                    </div>
                )
            }

            // nuevo 

            if (tipo.estatus == 2) {
                return (
                    <div className="row">
                        <div className="col-md-6">
                            <h1 style={{
                                "font-size": "24px",
                                'color': '#e149a4'
                            }}>Vocal sustituto</h1>
                            <label className="modal-mostar">Nuevo Registro</label>
                            <p>{sustitutoNombre}</p>
                            <label className="modal-mostar">Fecha de Ingreso:</label>
                            <p>{sustitutoFechaIngreso}</p>

                            <label className="modal-mostar">Fecha nombramiento:</label>
                            <p>{sustitutoFechaNombramiento}</p>
                            <label className="modal-mostar">Evidencia:</label>
                            <br></br>
                            {localStorage.getItem("evidencia02") != null ? <a style={{ 'color': '#e149a4' }} href={localStorage.getItem("evidencia02")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Evidencia.pdf</a> </a> : null}


                        </div>
                        <div className="col-md-6">
                            <h1 style={{
                                "font-size": "24px",
                                'color': '#e149a4'
                            }}>Vocal sustituido</h1>
                            <label className="modal-mostar">{sustituidoCargo}</label>
                            <p>{sustituidoNombre}</p>
                            <label className="modal-mostar">Fecha de Ingreso:</label>
                            <p>{sustituidoFechaIngreso}</p>

                            <label className="modal-mostar">Fecha nombramiento:</label>
                            <p>{sustituidoFechaNombramiento}</p>

                            <label className="modal-mostar">Nombramiento:</label>
                            <br></br>
                            {localStorage.getItem("evidencia02") != null ? <a style={{ 'color': '#e149a4' }} href={localStorage.getItem("evidencia02")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>nombrmiento.pdf</a> </a> : null}

                            <h3 style={{
                                'color': '#e149a4'
                            }}>Término en el cargo</h3>
                            <label className="modal-mostar">Fecha de término en el cargo:</label>
                            <p>{sustituidoFechaTemrinoCargo}</p>
                            <label className="modal-mostar">Motivo:</label>
                            <p>{sustituidoMotivo}</p>
                            <label className="modal-mostar">Evidencia:</label>
                            <br></br>
                            {localStorage.getItem("evidencia02") != null ? <a style={{ 'color': '#e149a4' }} href={localStorage.getItem("evidencia02")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Evidencia.pdf</a> </a> : null}

                        </div>
                    </div>
                )
            }
        }
    }

    const getFecha = (data) => {
        if (data != null) {
            let tempFechca = data.split(" ")[0].split("-");
            //let tempfecha= tempFechca.split("-");
            let fecha = tempFechca[2] + "/" + tempFechca[1] + "/" + tempFechca[0];
            return fecha;
        } else {
            return "N/A";
        }
        //return moment(new Date(data), "DD-MM-YYYY")
    }

    return (

        <>

            <br />

            {lista != null ?
                <>
                    <h1 style={{
                        "font-size": "24px",
                        'paddingLeft': '30px',
                    }}> <b>Vocales</b></h1>
                    <div className="row" style={{
                        'paddingLeft': '30px',
                        'top': '60px'
                    }}>
                        <br />
                        {
                            roles != null && roles.length > 0 && roles[0].idAccion == 1 ?
                                <Link to="/vocales/crear">
                                    <br />
                                    <Button className="btn btn-sq-lg btn-secondaryINE">
                                        <FileAddOutlined style={{ fontSize: '64px', color: 'black' }} /><br />
                                        Nuevo registro
                                        </Button>
                                </Link>

                                : null
                        }
                    </div>

                    <div className="row" >
                        <List style={{ background: "#fff" }}
                            itemLayout="horizontal"
                            locale={{ emptyText: 'No hay Vocales Registrados' }}


                            style={{ paddingLeft: "10%", paddingTop: "50px", width: "90%" }}
                            dataSource={lista}
                            renderItem={item => (

                                <List.Item>
                                    {/* {console.log("---",item)} */}
                                    <List.Item.Meta
                                        avatar={<Image
                                            width={80}
                                            height={80}
                                            alt="logo"
                                            src={buscarImg(item.id)}
                                            roundedCircle={true}
                                        />}
                                        title={<p >{item.cargo}</p>}

                                        description={<p >{item.nombre}</p>}
                                    />
                                    <div>

                                        {
                                            roles != null ?
                                                roles.map((tem) => (
                                                    <>
                                                        {
                                                            tem.idAccion === 2 && !item.incompleto ?
                                                                <Link to={{
                                                                    pathname: `${item.id}/mostrar`
                                                                }
                                                                } >
                                                                    {/* <i className='.ant-btn ant-btn-modifica' style={{ 'margin-right': '9px' }}>
                                                                <FormOutlined style={{ marginLeft: '9px' }} size={20} />                                                                                                      
                                                            </i> */}
                                                                    <Button
                                                                        key={JSON.stringify(item.id)}
                                                                        shape="circle"
                                                                        className=".ant-btn ant-btn-modifica"
                                                                        style={{ marginLeft: '15px' }}
                                                                    >
                                                                        <FormOutlined className="iconMostrar" size={20} />
                                                                    </Button>
                                                                </Link>
                                                                : null
                                                        }

                                                        {
                                                            tem.idAccion === 3 && !item.incompleto ?
                                                                <Link to={{
                                                                    pathname: `/vocales/${item.id}/editar`,
                                                                    query: { n: "rodrigo", f: [{ a: 2 }, { b: 2 }] }
                                                                }
                                                                } >
                                                                    {/* <i className='.ant-btn ant-btn-modifica' style={{ 'margin-right': '15px' }}>
                                                                            <FormOutlined style={{ marginLeft: '15px' }} size={20} />
                                                                             {console.log("que es item",item)} }
                                                        
                                                                        </i> */}
                                                                    <Button
                                                                        key={JSON.stringify(item.id)}
                                                                        shape="circle"
                                                                        className=".ant-btn ant-btn-modifica"
                                                                        style={{ marginLeft: '15px' }}
                                                                    >
                                                                        <FormOutlined className="iconEditarHome" size={20} />
                                                                    </Button>
                                                                </Link>
                                                                : null
                                                        }

                                                        {
                                                            tem.idAccion === 1 && !item.incompleto ?
                                                                <Link to={{
                                                                    pathname: `/vocales/${item.id}/remplazar`
                                                                }
                                                                } >
                                                                    <Button
                                                                        key={JSON.stringify(item.id)}
                                                                        shape="circle"
                                                                        className=".ant-btn ant-btn-modifica"
                                                                        style={{ marginLeft: '15px' }}
                                                                    >
                                                                        <FormOutlined className="iconSutituye" size={20} />
                                                                    </Button>
                                                                </Link>
                                                                : null
                                                        }
                                                        {
                                                            item.incompleto && item.incompleto ?
                                                                <Link to={{
                                                                    pathname: `/vocales/${item.id}/remplazar`
                                                                }
                                                                } >
                                                                    <Button
                                                                        key={JSON.stringify(item.id)}
                                                                        shape="circle"
                                                                        className=".ant-btn ant-btn-modifica"
                                                                        style={{ marginLeft: '15px' }}
                                                                    >
                                                                        <FormOutlined className="iconSutituye" size={20} />
                                                                    </Button>
                                                                </Link>
                                                                : null
                                                        }
                                                    </>
                                                )) :
                                                null
                                        }

                                    </div>
                                </List.Item>
                            )}
                        />
                    </div>
                    <br />
                    <br />
                    <div className="row" style={{ 'top': '60px', paddingLeft: "50px", paddingRight: "50px" }}>
                        <b>  <i className="iconClip" />Bitacora</b>
                        <br />
                    </div>
                    <div className="row" style={{ paddingLeft: "20px" }}>
                        {
                            bitacora != null ?
                                <Table dataSource={bitacora} style={{ paddingLeft: "30px", paddingRight: "30px", paddingTop: "30px", width: "100%" }} columns={columnas} />
                                : null
                        }
                    </div>
                    <div>
                        <Modal
                            visible={mostrar}
                            style={{ paddingLeft: "50px", paddingRight: "50px" }}
                            width={"70%"}
                            heigth={"30%"}
                            onCancel={cerrarModal}
                            footer={null}
                        >
                            {tipoSustitucion(tipoModal)}


                        </Modal>
                        {JSON.stringify(localStorage.getItem("nombramiento"))}

                        {/* {"valor de modal  " + incompeltoCargo}*/}

                        <a href={localStorage.getItem("nombramiento")} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>Nombramiento.pdf</a> </a>
                        <a href={evidencia01} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>evidencia 01.pdf</a> </a>
                        <a href={evidencia02} download  >   <i className="iconClip" /><a style={{ 'color': '#e149a4' }}>evnidenci02.pdf</a> </a>
                        <Button type="primary" onClick={() => descargaNombrmiento(163)}>
                            Open Modal
                        </Button>
                    </div>

                </>

                : null
                // <div className="containerLista container" >
                //     <div className="loading-style">
                //         <Spin indicator={antIcon} tip="Cargando..."></Spin>
                //     </div>
                // </div>
            }
        </>
    );
}
