import { insertarDomicilio, guardaArchivo } from "../../ActionsService/DomiciliosService";
import alertINE from "../../Components/Alert/AlertsINE";

export const convierteNo = (numero) => {
    let res = ''

    if (numero === '-1' | numero === 'null' | numero === null) {
        res = 'S/N'
    }
    else {
        res = numero
    }
    return res
}


export function getHiddenDivs(editar) {
    if (editar) {
        return ["", "hidden"];
    } else {
        return ["hidden", ""];
    }
}

export const sedesDisponibles = (sedes) => {
    let domicilios = (localStorage.getItem("listaDomicilios") !== 'undefined') ? JSON.parse(localStorage.getItem("listaDomicilios")) : null

    if (domicilios != null) {
        let idSedesOcupados = [-1, -2]
        domicilios.forEach((domicilio) => {
            idSedesOcupados.push(domicilio.sedes.idSede)
        })
        console.log("OCUPADOS " + idSedesOcupados)
        //regresa lista de Sedes Disponbiles
        return sedes.filter(sede => !idSedesOcupados.includes(sede.id));
    }
    else {
        return sedes
    }

}

export function enviar(props, history, domicilioEditar, menu, formularioSede,
    formularioCalle, formularioNumeroExterior, formularioNumeroInterior, formularioFotos,
    formularioCodigoPostal, formularioEntidad, formularioColonia, formularioMunicipio,
    calle, entidad, colonia, checkBoxNum, numeroExterior, numeroInterior, codigoPostal,
    municipio, domDentroDeJunta, apiCodigo, sede, listaSedes, tipoModulo, editar, match, fileList, fileNames
) {
    const listaSedes2 = [
        { id: 1, name: 'Bodega' },
        { id: 2, name: 'Módulo de Atención Ciudadana' },
        { id: 3, name: 'Junta Distrital' },
        { id: 4, name: 'V. de capacitación electoral y educación cívica' },
        { id: 5, name: 'V. de organización electoral' },
        { id: 6, name: 'V. del registro federal de electores' },
        { id: 7, name: 'V. del secretariado' },
        { id: 8, name: 'V. Ejecutiva' },
    ];
    console.log("SEDES ??????? ")
    console.log(listaSedes)
    const formData = new FormData();


    if (fileList != null) {
        fileList.forEach(function (value, i) { formData.append(('files'), value) })
    }



    // Display the key/value pairs
    for (var pair of formData.entries()) {
        console.log(pair[0] + ', ' + pair[1]);
    }


    console.log("EDITAR")
    console.log(domicilioEditar)
    console.log("ENVAIAR::::::")
    let dataNew = {}
    let tempDistrito = null;
    if (menu.infoMenu.distritoFedSelec != null) {
        //console.log(temp.distritoFedSelec.idDistrito)
        tempDistrito = menu.infoMenu.distritoFedSelec.idDistrito;
    }
    if (menu.infoMenu.distritoLocSelec != null) {
        tempDistrito = menu.infoMenu.distritoLocSelec.idDistrito;
    }
    console.log("ENTIDAD :::: " + entidad)

    console.log(dataNew);
    if (fileList != null) {
        console.log(fileList);
    }
    console.log(
        "fomularioeditar ´[" +
        (formularioSede || props.match.params.id != null) +
        " ] " +
        "formularioSede: [" +
        formularioSede +
        " ]" +
        "formularioCalle: [" +
        formularioCalle +
        " ]" +
        "formularioNumeroExterior: [" +
        formularioNumeroExterior +
        " ]" +
        "formularioNumeroInterior: [" +
        formularioNumeroInterior +
        " ]" +
        "formularioCodigoPostal: [" +
        formularioCodigoPostal +
        " ]" +
        "formularioEntidad: [" +
        formularioEntidad +
        " ]" +
        "formularioMunicipio: [" +
        formularioMunicipio +
        " ]" +
        "formularioColonia:[" +
        formularioColonia +
        " ]" +
        "formFotos " +
        formularioFotos
    );
    console.log("SEDE SELECCIONADA ==== " + sede)
    console.log(listaSedes2[sede])

    if (
        (formularioSede || props.match.params.id != null) &&
        formularioCalle &&
        formularioNumeroExterior | checkBoxNum &&
        formularioCodigoPostal &&
        formularioColonia &&
        formularioMunicipio &&
        formularioEntidad &&
        colonia != null

        // formularioFotos
    ) {
        dataNew = {
            idEstado: menu.infoMenu.estadoSelec.idEstado,
            idDistrito: tempDistrito,
            calle: calle,
            numeroExterior: checkBoxNum ? -1 : numeroExterior,
            numeroInterior: checkBoxNum ? -1 : numeroInterior,
            colonia: colonia,
            entidad: entidad,
            municipio: municipio,
            idMunicipio: '0',
            codigoPostal: codigoPostal,
            domDentroDeJunta: domDentroDeJunta,
            apiDireccion: apiCodigo ? 1 : 0,
            sedes: {
                idSede: sede,
                name: findId(listaSedes2, sede),
                usuario: 'miguel.garciac',
                fechaHora: '2019-07-04T20:38:38.604+00:00'
            },
            tipoModulo: tipoModulo === 0 ? 0 : 1,
            usuario: "miguel.garciac",
            fechaHora: "",
        };
        if (editar) {
            dataNew["idDirJuntaEjecutiva"] = match.match.params.id;
        }
        console.log("INSERTANDO")
        console.log(dataNew)



        let result = new Promise((resolve, reject) => {
            insertarDomicilio(dataNew)
                .then((response) => {
                    console.log("respuesta add ", response);
                    resolve(response.id)
                    if (response.error) {
                        alertINE("error", "No fue posible crear el registro.");
                        console.log("no se pudo agregar ");
                        console.log(response)
                        resolve(null)
                    } else {
                        console.log(response.data)
                        console.log("se agrego correctamente ");
                        console.log(response.id)
                        alertINE("success", "Registro agregado correctamente.");
                    }
                });
        })


        let aTerminadoSubidaArchivos = new Promise((resolve, reject) => {
            result.then((idDirJuntaEjecutiva) => {
                console.log("RESULT:::::::::::::::::" + idDirJuntaEjecutiva)
                console.log(idDirJuntaEjecutiva)
                if (fileList !== null && fileList.length !== 0) {
                    subirArchivo(getRutaArchivo(menu.infoMenu.estadoSelec.idEstado, tempDistrito, fileList, idDirJuntaEjecutiva, fileNames), history)
                }
                else {
                    resolve({ err: false });
                }
                //  window.location.replace("/sesiones/consejo/acreditacion/home");

            })
        })
        console.log("A TERMINADO ")
        console.log(aTerminadoSubidaArchivos)
        aTerminadoSubidaArchivos.then((result) => {
            console.log("RESULT ARCHIVOS _::::::::::::")
            console.log(result)
            if (!result.err) {
                history.push("/DomicilioJunta/Home")
            }
        })



    } else {
        alertINE("error", "Debes llenar todos los campos.");
        console.log("flatna campos ");
    }
}
function findId(data, idToLookFor) {
    var categoryArray = data;
    for (var i = 0; i < categoryArray.length; i++) {
        if (categoryArray[i].id === idToLookFor) {
            return (categoryArray[i].name);
        }
    }
}
export const download = (data, filename, type) => {
    var file = new Blob([data], { type: type });
    if (window.navigator.msSaveOrOpenBlob)
        // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else {
        // Others
        var a = document.createElement("a"),
            url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function () {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
};

const asignaNombreaNuevosArchivos = (compressedImages, previuosNames) => {
    const allNames = ['0.jpg', '1.jpg', '2.jpg']
    let availableNames = allNames.filter(name => !previuosNames.includes(name))
    console.log("AVAI NAMES " + availableNames)

    let resultNames = []
    for (var i = 0; i < compressedImages.length; i++) {
        resultNames.push(availableNames[i])
    }
    console.log("RESULT NAMES::::: ")
    console.log(resultNames)

    return resultNames
}
const getRutaArchivo = (idEstado, idDistrito, listaArchivos, idDirJuntaEjecutiva, previuosNames) => {
    const formData = new FormData();
    let listaNombres = []

    if (previuosNames != null) {
        listaNombres = asignaNombreaNuevosArchivos(listaArchivos, previuosNames)
        for (const [i] of listaArchivos.entries()) {
            formData.append('files', listaArchivos[i])
        }
    } else {
        for (const [i, value] of listaArchivos.entries()) {
            formData.append('files', listaArchivos[i])
            listaNombres.push(i + '.jpg')
            console.log('%d: %s', i, value);
        }
    }
    // listaArchivos.forEach((archivo) => {
    //     formData.append('files', archivo)
    //     listaNombres.push(archivo.name)
    // }
    // )
    console.log(listaArchivos)
    console.log(listaNombres)

    formData.append('param', new Blob([JSON.stringify({
        idEstado: idEstado,
        idDistrito: idDistrito,
        modulo: "Domicilios",
        idSubModulo: Number(idDirJuntaEjecutiva),
        nombreSistema: "junta",
        modificar: true,
        nombreArchivo: listaNombres,
    })], { type: "application/json" }));
    return formData;
}

const subirArchivo = (multipart, history) => {
    guardaArchivo(multipart).then((response) => {
        console.log("Respuesta Servicio Carga de Archivos: " + JSON.stringify(response));
        if (response.data.code !== 200 || response.data.code === null) {
            alertINE("error", "No fue posible guardar las fotografías del domicilio. Intente más tarde");
            console.log("no se pudo agregar");
        } else {
            //insertarDomicilio(DTODomicilio);
            history.push("/DomicilioJunta/Home")
            console.log("FOTOS CARGADAS CORRECTAMENTE")
        }
    });
}