import React, { useEffect } from 'react'
import { BrowserRouter as Router, Route } from "react-router-dom";
import Domicilios from "./Domicilios";
import Comisiones from "./Comisiones"
import { storeDomicilios, storeLoading } from "../../Reducers/HomeRed";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose } from "redux";
// import Formulario from "../PaginasContenedores/Vocales/VocalesFormulario";
import CrearDomicilio from "./CrearDomicilio";
//import Imagen from../../Components/CargarImagen/imageneses";
const storeDomiciliosJunta = createStore(storeDomicilios, compose(applyMiddleware(thunk)))
const storeLoad = createStore(storeLoading, compose(applyMiddleware(thunk)))
export default function DomiciliosHome(props) {
    let { dataMenu } = props;
    // console.log("STORE HOME DOMICILIOS ")
    // console.log(props)
    const rolUsuario = dataMenu.rolUsuario

    useEffect(() => {
        // console.log("STORE DOM:::::")
        // console.log(storeDomicilios)
    }, []);

    return (
        <div>
            <Route
                path="/DomicilioJunta/Home"
                component={({ match }) => (
                    <Domicilios match={match} menu={props.dataMenu} store={storeLoad} rolUsuario={rolUsuario} storeDomicilios={storeDomiciliosJunta} />
                )}
            />
            <Route
                path="/DomicilioJunta/Captura"
                component={({ match }) => (
                    <CrearDomicilio match={match} menu={props.dataMenu} store={storeLoad} rolUsuario={rolUsuario} editar={false} />
                )}
            />
            <Route
                path={`/DomicilioJunta/:id/editar`}
                component={(match) => (
                    <CrearDomicilio
                        match={match}
                        menu={dataMenu}
                        store={storeLoad}
                        editar={true}
                        storeDomicilios={storeDomiciliosJunta}
                        rolUsuario={rolUsuario}
                    />
                )}
            />
        </div>
    )
}