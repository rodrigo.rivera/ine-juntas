import React, { useState } from "react";
import { Button, Divider } from "antd";
import { FormOutlined, DeleteOutlined } from "@ant-design/icons";
import { List, Avatar } from "antd";
import { Link } from "react-router-dom";
import alertINE from "../../Components/Alert/AlertsINE";
import { deleteDomicilio } from "../../ActionsService/DomiciliosService";
import defaultLogo from "./assets/img/SinDomicilio.svg";
import Carousel, { Modal, ModalGateway } from 'react-images';
import DialogoINE from './DialogoINE'

export default function ListaPartidos(props) {
  let { store } = props;
  // console.log("props++++" + props.partidos);
  const [listaActualPartidos, setListaActualPartidos] = useState(
    props.listaActualPartidos != null ? props.listaActualPartidos : []
  );
  const [openDialog, setOpenDialog] = useState(false)
  const [idEliminar, setIdEliminar] = useState(null)
  const [modalIsOpen, setModalIsOpen] = useState(false)
  //TODO HABILITAR CARGA DE IMAGENES
  const [imagenesParaVer, setImagenesParaVer] = useState(null)
  const [fotos] = useState(props.fotos)

  const [permisos] = useState(props.permisos);
  const [nombreSede, setNombreSede] = useState(null)

  let locale = {
    emptyText: (
      <span>
        <p style={{ fontSize: "16px" }}>
          {/* <Icon type="like" /> */}
          No hay Domicilio de Junta registrados
        </p>
        {/* <Button>Custom Button</Button> */}
      </span>
    ),
  };


  const editar = (idDirJuntaEjecutiva) => {
    const domicilioEditar = listaActualPartidos.filter(item => item.idDirJuntaEjecutiva === idDirJuntaEjecutiva);
    domicilioEditar[0]["images"] = getImagesOfSomeDomicilioJunta(idDirJuntaEjecutiva, fotos)

    console.log("PARA EDITAR :::::::::::::: ")
    console.log(domicilioEditar)

    store.dispatch({ type: "DOMICILIO_LOADED", domicilioEditar: domicilioEditar[0] });
    localStorage.setItem("domicilioEditar", JSON.stringify(domicilioEditar))
    // console.log("STORE DESDE LISTA PARTIDOS")
    // console.log(store)
  }

  const confirmaEliminar = (id, nombreSede) => {
    setOpenDialog(true)
    setIdEliminar(id)
    setNombreSede(nombreSede)
  }
  const confirma = () => {
    // console.log("CONFIRMA ELIMINAR")
    setOpenDialog(false)
    eliminar(idEliminar)
  }
  const cancelar = () => {
    setOpenDialog(false)
    alertINE('info', 'Se ha cancelado el proceso de eliminación.')
    // console.log("CANCELAR ELIMINAR")
  }

  const Dialogo = () => {
    return (
      <div >
        <DialogoINE
          handleAceptar={confirma}
          handleCancelar={cancelar}
          title={"Eliminar " + nombreSede}
          content={"¿Estás seguro de eliminar el domicilio de Junta?"}
          openDialog={openDialog} />
      </div>
    )
  }

  const eliminar = (idDirJuntaEjecutiva) => {
    deleteDomicilio(idDirJuntaEjecutiva).then((response) => {
      if (!response.error) {
        alertINE("success", "Registro eliminado correctamente.");
        eliminaDomicilioDeListaActual(idDirJuntaEjecutiva)
      } else {
        alertINE("error", "Ha ocurrido un error al eliminar el domicilio de junta, intenta nuevamente.");
      }
    });
  };

  const eliminaDomicilioDeListaActual = (idDirJuntaEjecutiva) => {
    const items = listaActualPartidos.filter(item => item.idDirJuntaEjecutiva !== idDirJuntaEjecutiva);
    setListaActualPartidos(items)
    localStorage.setItem("listaDomicilios", JSON.stringify(items))
    // console.log(items)
    // console.log(listaActualPartidos)
  }
  const sedes = [
    { id: 1, name: 'Bodega' },
    { id: 2, name: 'Módulo de Atención Ciudadana' },
    { id: 3, name: 'Junta Distrital' },
    { id: 4, name: 'V. de capacitación electoral y educación cívica' },
    { id: 5, name: 'V. de organización electoral' },
    { id: 6, name: 'V. del registro federal de electores' },
    { id: 7, name: 'V. del secretariado' },
    { id: 8, name: 'V. Ejecutiva' },
  ];

  const toggleModal = (item) => {
    if (fotos !== null) {
      let images = getImagesOfSomeDomicilioJunta(item.idDirJuntaEjecutiva, fotos)
      console.log("ASDHASHDKHASHDJAHSJDHJAHDJHAJDH")
      console.log(images)

      setImagenesParaVer(images)
      console.log("PARA VER::::::::::::")
      console.log(images)

      setModalIsOpen(modalIsOpen ? false : true)
      console.log("QUIERO VER ")
      console.log((item))
      console.log(modalIsOpen)
    } else {
      alertINE('info', 'No se han cargado fotografías para este Domicilio')
    }
  }

  const getImagesOfSomeDomicilioJunta = (idDirJuntaEjecutiva, jsonImages) => {
    let fotosDirJunta = {
      sources: []
    }

    for (var i = 0; i < jsonImages.blobs.length; i++) {
      if (jsonImages.blobs[i].name.includes(idDirJuntaEjecutiva)) {
        // fotosDirJunta["source"] = 'data:image/jpg;base64' + jsonImages[i].archivoBase64;
        // var url = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
        fotosDirJunta.sources.push({
          "source": jsonImages.blobs[i].source,
          "url": jsonImages.blobs[i].source,
          "uid": jsonImages.blobs[i].order,
          "name": jsonImages.blobs[i].order + '.jpg',
          "status": 'done'
        })
      }
    }
    console.log(fotosDirJunta)
    return fotosDirJunta.sources
  }

  function getCaratula(data, idToLookFor) {
    console.log("BUSCAR " + idToLookFor)
    console.log(data)
    var categoryArray = data.blobs;
    for (var i = 0; i < categoryArray.length; i++) {
      if (categoryArray[i].name === idToLookFor.toString() && categoryArray[i].order === '0') {
        console.log("caratula " + categoryArray[i].name)
        return (categoryArray[i].source);
      }
    }
  }

  return (
    <div className="card mb-3" style={{ borderColor: "transparent" }}>
      <div className="row no-gutters">
        {/* <p>lista{paginaActual}</p> */}
        {props.partidos === null ? null :
          <Divider style={{ top: "23px" }} />
        }
        <ModalGateway>
          {modalIsOpen ? (
            <Modal allowFullscreen={false} onClose={() => setModalIsOpen(false)}>
              <Carousel views={imagenesParaVer} />
            </Modal>
          ) : null}
        </ModalGateway>
        <List
          locale={locale}
          itemLayout="horizontal"
          dataSource={listaActualPartidos}
          size="large"
          renderItem={(item) => (
            <List.Item key={item.idAsociacion}>
              {/* {console.log('id ********** '+JSON.stringify(item))} */}
              <List.Item.Meta
                //avatar={<Avatar shape="square" size={80} src={item.image} onClick={() => toggleModal(item)} />}
                avatar={<Avatar shape="square" size={80} src={fotos !== null ? getCaratula(fotos, item.idDirJuntaEjecutiva) : defaultLogo} onClick={() => toggleModal(item)} />}
                title={
                  <div
                    className="ant-list-item-meta-title"
                    href={item.idDomicilioAsociacion + "/editar"}
                    style={{ color: "#333333" }}
                  >
                    <strong>
                      {sedes[(item.sedes.idSede - 1)].name.replace('V.', 'Vocalía')}
                    </strong>
                  </div>
                } //item.nombreAsociacion
                description={
                  <div>
                    <p-lista-partidos>
                      {item.calle}{", "}
                      {(item.numeroExterior === "0") ||
                        (item.numeroExterior === "-1") ||
                        (item.numeroExterior === null)
                        ? "S/N "
                        : item.numeroExterior}{" "}

                      {(item.numeroInterior === null | item.numeroInterior === '-1')
                        ? "Int: S/N"
                        : "Int: " + item.numeroInterior}
                      {", "}
                      {item.colonia} C.P. {item.codigoPostal} {", "}{item.municipio}{", "}{item.entidad}
                    </p-lista-partidos>
                    <br />
                    {/* SÓLO MOSTRAR SI Es MÖDULO DE ATENCIÓN START*/}
                    {/* <p-lista-partidos>
                      <strong>Teléfono: </strong> {item.telefono}
                    </p-lista-partidos> */}
                    {/* SÓLO MOSTRAR SI Es MÖDULO DE ATENCIÓN END*/}
                  </div>
                }
              />
              {/* {console.log('id :::: '+item.idAsociacion)} */}
              {/* <Button className='.ant-btn ant-btn-modifica' onClick={onClick}> */}
              {
                permisos != null
                  ? permisos.map(
                    (temp) =>
                      temp.idAccion === 3 && (
                        <div style={{ paddingLeft: "150px" }}>
                          <Link to={{ pathname: `${item.idDirJuntaEjecutiva}/editar`, }}>
                            <Button
                              type="secondary"
                              key={"JSON.stringify(temp)"}
                              shape="circle"
                              className="ant-btn ant-btn-modifica"
                              onClick={() => editar(item.idDirJuntaEjecutiva)}
                            >
                              <FormOutlined size={35} />
                            </Button>
                          </Link>
                        </div>
                      )
                  ) : null
              }
              {
                permisos != null
                  ? permisos.map(
                    (temp) =>
                      temp.idAccion === 3 && (
                        <div style={{ paddingLeft: "15px" }}>
                          <Button
                            type="secondary"
                            key={"JSON.stringify(temp)"}
                            shape="circle"
                            className=".ant-btn ant-btn-modifica"
                            onClick={() => confirmaEliminar(item.idDirJuntaEjecutiva, item.sedes.sede)}
                          >
                            <DeleteOutlined size={35} />
                          </Button>
                        </div>
                      )
                  ) : null
              }
            </List.Item>
          )}
        />
        {props.partidos === null ? null :
          <Divider style={{ top: "-23px" }} />
        }
        <div >
          {Dialogo()}
        </div>
      </div>
    </div>
  );
}
