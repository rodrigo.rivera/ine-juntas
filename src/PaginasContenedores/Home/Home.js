import React, { useState, useEffect, useReducer } from "react";
import { fetchLogin } from "../../ActionsService/HomeActionService";
import ProcessMenu from "../../Components/ProcessMenu/ProcessMenu";
import { Layout, Icon, Spin } from "antd";
import { storeMenu } from "../../Reducers/HomeRed";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import Menu from "../../Components/Sider/Sider";
import { Route, Switch, Link } from "react-router-dom";
import Footer from "../../PaginasContenedores/ContenedorGeneral/Footer/Footer";
import Header from "../../PaginasContenedores/ContenedorGeneral/Header/Header";
// import Acreditacion from "../../PaginasContenedores/Acreditacion/Acreditacion";
// import CrearAcreditacion from "../../PaginasContenedores/Acreditacion/CrearAcreditacion";
import { ReactComponent as Heder_Logo } from "../../Assets/Imagenes/Header/Logo.svg";
import { ReactComponent as Heder_Logo_User } from "../../Assets/Imagenes/Header/user.svg";
// import { getAsociaciones } from "../../ActionsService/AcreditacionService";
// import { reducerListaPartidos } from "../../Reducers/Acreditacion";

import jwtDecode from "jwt-decode";
const antIcon = <Icon type="loading" style={{ fontSize: 30 }} spin />;
const { Content } = Layout;

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => ({
  fetchLogin: (data, accessToken, menu) =>
    dispatch(fetchLogin(data, accessToken, menu)),
});

const store = createStore(storeMenu, compose(applyMiddleware(thunk)));
const StoreContext = React.createContext(null);

export default function Home(props) {
  //console.log("los props", props);

  let { data, match } = props;
  const [info] = useState(data);

  const [dataMenu, setDataMenu] = useState(null);


  const dataResponse = {
    infoMenu: {
      detalleSelec: info.infoMenu.detalleSelec,
      listaDetalles: info.infoMenu.listaDetalles,
      estadoSelec: info.infoMenu.estadoSelec,
      listaEstados: info.infoMenu.listaEstados,
      distritoFedSelec: info.infoMenu.distritoFedSelec,
      listaDistritosFed: info.infoMenu.listaDistritosFed,
      distritoLocSelec: info.infoMenu.distritoLocSelec,
      listaDistritosLoc: info.infoMenu.listaDistritosLoc,
      listaDistritosFedAux: info.infoMenu.listaDistritosFedAux,
      municipioSelec: info.infoMenu.municipioSelec,
      listaMunicipios: info.infoMenu.listaMunicipios,
      listMenu: info.infoMenu.listMenu,
    },
    idSistema: info.idSistema,
    idEstado: info.idEstado,
    idDistrito: info.idDistrito,
    idMunicipio: info.idMunicipio,
    ambito: info.ambito,
    rolUsuario: info.rolUsuario,
    versionUsuario: info.versionUsuario,
    tipoCambioGeografico: null,
    tipoSistema: info.tipoSistema,
    idCorte: info.idCorte,
  };

  /**
   * estado Redux
   */
  // store.subscribe(() => {
  //   let respuestaStore = store.getState();
  //   //console.log("redux home ", respuestaStore);

  //   if (
  //     !respuestaStore.isLoading &&
  //     axuliarDistrito(respuestaStore.menu) != null
  //   ) {
  //     //console.log("Home ", respuestaStore.menu.infoMenu);
  //     setDataMenu(respuestaStore.menu.infoMenu);
  //   }
  // });

  /**
   * verifica que aya distrito
   * @param {*} menu
   */
  const axuliarDistrito = (menu) => {
    let tempDistrito = null;
    //console.log("auxiliar ", menu.infoMenu)
    if (menu.infoMenu.distritoFedSelec != null) {
      //console.log("entro a el axuli ",)
      tempDistrito = menu.infoMenu.distritoFedSelec.idDistrito;
    }
    if (menu.infoMenu.distritoLocSelec != null) {
      tempDistrito = menu.infoMenu.distritoLocSelec;
    }
    if (menu.infoMenu.municipioSelec != null) {
      tempDistrito = menu.infoMenu.municipioSelec;
    }
    return tempDistrito;
  };



  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header
        Logo={Heder_Logo}
        LogoUser={Heder_Logo_User}
        User={jwtDecode(props.accessToken).sub}
        inicio=""
        data={data}
        accessToken={props.accessToken}
      />

      {/* <Layout>
        {dataMenu != null ? <Menu menu={dataMenu.listMenu} /> : null}
        <Content style={{ background: "#fff", paddingBottom: "2.5rem" }}>
          <ProcessMenu store={store} dataMenu={dataResponse} />
          

         
        </Content>
      </Layout> */}
      <Footer className="footer" />
    </Layout>
  );
}
