import * as ActionTypes from '../Actions/Actions';

export const loginRed = (state = {}, action) => {
    switch (action.type) {
        case ActionTypes.LOGIN_SISTEMA:
            return { ...state, isLogged: action.isLogged, data: action.data, accessToken: action.accessToken, menu: action.menu, idSistema: action.idSistema, messageError: action.messageError }
        case ActionTypes.LOGOUT_SISTEMA_ERROR:
            return { ...state, messageError: action.messageError }
        case ActionTypes.LOGOUT_SISTEMA:
            return { ...state = undefined }
        default:
            return state;
    }
}

export function storeMenu(state = null, action) {
    switch (action.type) {
        case ActionTypes.MENU_LOADING:
            return state = { isLoading: true, menu: [] };
        case ActionTypes.MENU_LOADED:
            return state = { isLoading: false, menu: action.menu };

            // case ActionTypes.OC:
            return null;

    }

}

export function storeLoading(state = null, action) {
    switch (action.type) {
        case ActionTypes.LOADING:
            return state = { pending: true };
        case ActionTypes.LOADED:
            return state = { pending: false };
    }
}

export function storeDomicilios(state = null, action) {
    switch (action.type) {
        case ActionTypes.DOMICILIO_LOADING:
            return state = { pending: true, domicilioEditar: null };
        case ActionTypes.DOMICILIO_LOADED:
            return state = { pending: false, domicilioEditar: action.domicilioEditar };
    }
}