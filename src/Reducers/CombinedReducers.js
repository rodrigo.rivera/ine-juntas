import { combineReducers } from 'redux';
import * as ActionTypes from '../Actions/Actions';
import { loginRed } from './HomeRed';

const appReducer = combineReducers({
    inicio: loginRed
})

const rootReducer = (state, action) => {

    if (action.type === ActionTypes.LOGOUT_SISTEMA) {

        state = undefined

    }

    return appReducer(state, action)
}

export default rootReducer;