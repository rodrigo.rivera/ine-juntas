# SESIONES DE JUNTA v1.0
-----------------------

## Info
proyect name: Sesiones de Junta V1.0
author: Manuel Torres
mail: manuel.torres@ine.mx
created: 2020 - March
Last Update: 2020 - March - 19

### Instalacion  

>Requiere [Node.js](https://nodejs.org/) con la version minima o superior a v12.14.1+ 

>Requiere [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable) con la version minima o superior a v1.22.0+ 

Instalar las dependencias y ejecuatar 

```sh
$cd ine-react 
$yarn install 
$yarn start 
```

